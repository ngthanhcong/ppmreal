<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check())
    {
        Auth::logout();
        return view('auth.login');
        
    }
    return view('auth.login');
});

Auth::routes(['active' => 1]);
    Route::resource('lich-su-giao-dich', 'TransactionHistoriesController');
    // Route::resource('sale', 'SaleController');
    Route::resource('lich-su-tu-van', 'HistoriesController');
    Route::get('/profile', 'UserController@profile')->name('profile');
    Route::put('/profileUpdate/{id}', 'UserController@profileUpdate')->name('profileUpdate');
    Route::resource('sale', 'SaleController');
// Middleware Admin
Route::group(['middleware' => 'role:Admin'], function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('usergroup', 'UserGroupController');
    Route::resource('category', 'CategoryController');
    Route::resource('product', 'ProductController');
    Route::resource('user', 'UserController');
    Route::resource('permission', 'PermissionController');  
    Route::resource('role', 'RoleController');
    Route::put('/activeStatus/{id}', 'UserController@activeStatus')->name('activeStatus');
 });
// Middleware Employee
Route::group(['middleware' => 'role:Employees'], function() {
    Route::resource('user-sale', 'UserSalesController');
    
});
// Route::resource('customer', 'CustomerController');
Route::get('/map', function () {
    return view('test');
});


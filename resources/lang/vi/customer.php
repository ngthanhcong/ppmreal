<?php

return [
    'customer_name'         => 'Họ và tên',
    'customer_address'      => 'Địa chỉ',
    'customer_email'        => 'Email',
    'customer_gender'       => 'Giới tính',
    'customer_phone'        => 'Số điện thoại',
];
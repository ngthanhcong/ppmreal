<?php

return [
    'create_category'       => 'Thêm mới danh mục bất động sản',
    'category_name'         => 'Tên danh mục',
    'note'                  => 'Ghi chú',
    'add'                   => 'Thêm mới',
    'save'                  => 'Lưu lại',
    'cancel'                => 'Hủy bỏ',
    'edit_category'         => 'Chỉnh sửa danh mục bất động sản',
    'action'                => 'Action',
    'delete_category'       => 'Bạn chắc chắn muốn xoá dữ liệu này?',
    'delete'                => 'Xóa',
];
<?php

return [
    'create_product'        => 'Thêm mới bất động sản',
    'add'                   => 'Thêm mới',
    'save'                  => 'Lưu lại',
    'cancel'                => 'Hủy bỏ',
    'edit_product'          => 'Chỉnh sửa bất động sản',
    'action'                => 'Action',
    'delete_product'        => 'Bạn chắc chắn muốn xoá dữ liệu này?',
    'delete'                => 'Xóa',
    'category_id'           => 'Loại bất động sản',
    'ward_id'               => 'Vị trí',
    'user_id'               => 'Người tạo',
    'product_name'          => 'Tên sản phẩm',
    'alias'                 => 'Định danh',
    'avatar'                => 'Ảnh đại điện',
    'alt'                   => '',
    'images'                => '',
    'summary'               => 'Mô tẳ ngắn',
    'content'               => 'Nội dung',
    'price'                 => 'Giá',
    'acreage'               => 'Diện tích',
    'area'                  => 'Mảng các điểm mốc của sản phẩm',
    'length'                => 'Chiều dài',
    'width'                 => 'Chiều rộng',
    'direction'             => 'Hướng',
    'status'                => 'Trạng thái',
    'lat'                   => 'Vĩ độ',
    'lng'                   => 'Kinh độ',
    'note'                  => 'Ghi chú',
    'back'                  => 'Trở lại',
    "directions" => [
        [
            "vi" => "Đông",
            "en" => "The East",
        ],
        [
            "vi" => "Tây",
            "en" => "The West",
        ],
        [
            "vi" => "Nam",
            "en" => "The South",
        ],
        [
            "vi" => "Bắc",
            "en" => "The North",
        ],
        [
            "vi" => "Đông Nam",
            "en" => "East South",
        ],
        [
            "vi" => "Đông Bắc",
            "en" => "East North",
        ],
        [
            "vi" => "Tây Bắc",
            "en" => "West North",
        ],
        [
            "vi" => "Tây Nam",
            "en" => "West South",
        ],
    ],
    "cost-fields" => [
        "vi" => [
            [
                "min" => "500000000",
                "max" => "1000000000",
            ],
            [
                "min" => "1000000000",
                "max" => "2500000000",
            ],
            [
                "min" => "2500000000",
                "max" => "5000000000",
            ],
            [
                "min" => "5000000000",
                "max" => "8000000000",
            ],
            [
                "min" => "8000000000",
                "max" => "11000000000",
            ],
        ],
        "en" => [
            [
                "min" => "5000",
                "max" => "10000",
            ],
            [
                "min" => "10000",
                "max" => "20000",
            ],
            [
                "min" => "20000",
                "max" => "50000",
            ],
            [
                "min" => "50000",
                "max" => "80000",
            ],
            [
                "min" => "80000",
                "max" => "110000",
            ],
        ],
        "unit_vi" => "VND",
        "unit_en" => "USD",
    ],
    "price-conversion" => [
        1 => '',
        2 => '',
        3 => 'trăm',
        4 => 'nghìn',
        5 => 'nghìn',
        6 => 'nghìn',
        7 => 'triệu',
        8 => 'triệu',
        9 => 'triệu',
        10 => 'tỷ',
        11 => 'tỷ',
        12 => 'tỷ',
        13 => 'nghìn tỷ',
        14 => 'nghìn tỷ',
        15 => 'nghìn tỷ',
    ],
    "acres" => [
        "acreages" => [
            [
                "min" => "0",
                "max" => "30",
            ],
            [
                "min" => "30",
                "max" => "50",
            ],
            [
                "min" => "50",
                "max" => "70",
            ],
            [
                "min" => "70",
                "max" => "100",
            ],
            [
                "min" => "100",
                "max" => "150",
            ],
            [
                "min" => "150",
                "max" => "200",
            ],
            [
                "min" => "200",
                "max" => "250",
            ],
            [
                "min" => "250",
                "max" => "300",
            ],
            [
                "min" => "300",
                "max" => "350",
            ],
            [
                "min" => "350",
                "max" => "400",
            ],
            [
                "min" => "400",
                "max" => "600",
            ],
            [
                "min" => "599",
                "max" => "800",
            ],
            [
                "min" => "800",
                "max" => "1000",
            ],
            [
                "min" => "1000",
                "max" => "5000",
            ],
        ],
        "length" => [
            [
                "min" => "9",
                "max" => "14",
            ],
            [
                "min" => "14",
                "max" => "16",
            ],
            [
                "min" => "16",
                "max" => "20",
            ],
            [
                "min" => "20",
                "max" => "25",
            ],
        ],
        "width" => [
            [
                "min" => "4",
                "max" => "6",
            ],
            [
                "min" => "6",
                "max" => "8",
            ],
            [
                "min" => "8",
                "max" => "11",
            ],
            [
                "min" => "11",
                "max" => "16",
            ],
        ],
    ],
];
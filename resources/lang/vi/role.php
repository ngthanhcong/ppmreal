<?php

return [
    'role_name'        => 'Tên nhóm',
    'note'             => 'Chú thích',
    'created_at'       => 'Ngày tạo',
    'updated_at'       => 'Ngày cập nhật',
    'role_create'      => 'Thêm nhóm mới',
    'role_del'         => 'Bạn chắc chắn muốn xoá dữ liệu này?',
    'save'             => 'Lưu lại',
    'delete'           => 'Xóa bỏ',
    'cancel'           => 'Hủy bỏ',
    'back'             => 'Trở lại',
    'edit_role'        => 'Chỉnh sửa nhóm',
];
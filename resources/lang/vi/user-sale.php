<?php

return [
    'fullname'          => 'Tên đầy đủ',
    'username'          => 'Tên đăng nhập',
    'password'          => 'Mật khẩu',
    'avatar'            => 'Ảnh đại diện',
    'gender'            => 'Giới tính',
    'birthday'          => 'Ngày sinh',
    'phone'             => 'Số điện thoại',
    'email'             => 'Email',
    'address'           => 'Địa chỉ',
    'active'            => 'Trạng thái',
    'user_sale_create'  => 'Thêm nhân viên cấp dưới',
    'infomation'        => 'Nhập thông tin ',
    'user_sale_del'     => 'Bạn chắc chắn muốn xoá dữ liệu này?',
    'save'              => 'Lưu lại',
    'delete'            => 'Xóa bỏ',
    'cancel'            => 'Hủy bỏ',
    'back'              => 'Trở lại',
    'edit_user_sale'    => 'Chỉnh sửa thông tin',
    'create'            => 'Tạo mới',
    'list_staff'        => 'Danh sách nhân viên',
    'update_profile'    => 'Cập nhật thông tin',
    
];
<?php

return [
    'permission_name'    => 'Tên quyền',
    'note'               => 'Chú thích',
    'created_at'         => 'Ngày tạo',
    'updated_at'         => 'Ngày cập nhật',
    'per_create'         => 'Thêm quyền mới',
    'per_del'            => 'Bạn chắc chắn muốn xoá dữ liệu này?',
    'save'               => 'Lưu lại',
    'delete'             => 'Xóa bỏ',
    'cancel'             => 'Hủy bỏ',
    'back'               => 'Trở lại',
    'permission_edit'    => 'Chỉnh sửa quyền'
];
<?php

return [
    'create_user'           => 'Thêm mới nhóm người dùng',
    'group_name'            => 'Tên nhóm',
    'note'                  => 'Ghi chú',
    'add'                   => 'Thêm mới',
    'save'                  => 'Lưu lại',
    'cancel'                => 'Hủy bỏ',
    'edit_user'             => 'Chỉnh sửa nhóm người dùng',
    'action'                => 'Action',
    'delete_user'           => 'Bạn chắc chắn muốn xoá dữ liệu này?',
    'delete'                => 'Xóa',
];
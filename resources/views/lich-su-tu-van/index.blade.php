@extends('layouts.master')
@section('title')
 {{ __('users.histories_advisory')  }}
@stop
@php
    function user_name($user_id)
    {
        $user_name = \App\User::where('id', $user_id)->first();
        return $user_name->fullname;
    }
    function customer_name($customer_id)
    {
        $customer_name = \App\Customer::where('id', $customer_id)->first();
        return $customer_name->customer_name;
    }
    function product_name($product_id)
    {
        $product_name = \App\Product::where('id', $product_id)->first();
        return $product_name->product_name;
    }
    function customer($customer_id)
    {
        $customer = \App\Customer::where('id', $customer_id)->first();
        return $customer;
    }      

    function product($product_id)
    {
        $product = \App\Product::where('id', $product_id)->first();
        return $product;
    }
    function get_address($ward_id)
    {
      $ward = \App\Ward::where('id', $ward_id)->first();
      $district = \App\District::where('id', $ward->district_id)->first();
      $town = \App\Town::where('id', $district->town_id)->first();
      $address = $ward->ward_name .', '. $district->district_name .', '. $town->town_name ;
      return $address;
    }
    function get_category($category_id)
	  {
      $category = \App\Category::where('id', $category_id)->first();
      return $category->category_name;
	  }
@endphp



@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.histories_management')}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.histories_advisory') }}</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.histories_advisory') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>{{ __('history.user_name') }}</th>
                  <th>{{ __('history.customer_name') }}</th>
                  <th>{{ __('history.product_name') }}</th>
                  <th>{{ __('history.consulting_status') }}</th>
                  <th>{{ __('history.consulting_at') }}</th>
                  <th style="width:100px">{{ __('history.note') }}</th>
                  <!-- <th>Action</th> -->
                </tr>
                </thead>
                <tbody>
                @foreach ($histories as $history)
                @php
                  $customer = customer($history->customer_id);
                  $product = product($history->product_id);
                @endphp
                <tr>
                  <td>{{ user_name($history->user_id) }}</td>
                  <td>{{ customer_name($history->customer_id) }}</td>
                  <td>
                    <a style="color:black" data-toggle="tooltip" title="{{ __('sales.view') }}" href="{{ route('lich-su-tu-van.show',$history->id) }}" >
                    {{ product_name($history->product_id) }}
                    </a>
                  </td>
                  <td style="font-size:18px"> 
                  @if ($history->consulting_status == 0)
                  <span class="badge bg-info" data-toggle="tooltip" title="{{ __('sales.confirm') }}">
                    <a style="text-decoration:none; cursor: pointer;" data-toggle="modal" data-target="#modalUpdateForm">
                    {{ __('history.selling_history') }}
                    </a>
                  </span>                    
                  @elseif ($history->consulting_status == 1)
                    <span class="badge bg-success">{{ __('history.successful_advice') }}</span>
                  @elseif ($history->consulting_status == 2)
                    <span class="badge bg-danger">{{ __('history.consulting_failed') }}</span>
                  @endif
                  </td>
                  <td>{{ date('d/m/Y H:i:s', strtotime($history->consulting_at)) }}</td>
                  <td style="width:100px">{{ $history->note }}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
<div class="modal fade bd-example-modal-lg" id="modalUpdateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header card-primary">
            <!-- <h4 class="modal-title w-100 font-weight-bold">Sign up</h4> -->
            <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('history.fill_infomation') }}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body mx-3">
      <form method="post" action="{{ route('lich-su-giao-dich.store') }}">
                  @csrf
                  @method('POST')
                  <div class="row">
                    <div class="col">
                    <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('history.input_infomation') }}</h3>
                    <table style="margin-left:20px;">
                      <tr class="form-group">
                        <td ><b>{{ __('customer.customer_name') }}</b></td>
                        <td style="padding:5px"> <input readonly id="customer_name" type="text" class="form-control{{ $errors->has('customer_name') ? ' is-invalid' : '' }}" name="customer_name" value="{{ $customer->customer_name }}" autofocus></td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>{{ __('customer.customer_address') }}</b></td>
                        <td style="padding:5px"><input id="customer_address" type="text" class="form-control{{ $errors->has('customer_address') ? ' is-invalid' : '' }}" name="customer_address" value="{{$customer->customer_address}}" autofocus></td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>{{ __('customer.customer_email') }}</b></td>
                        <td style="padding:5px"><input id="customer_email" type="text" class="form-control{{ $errors->has('customer_email') ? ' is-invalid' : '' }}" name="customer_email" value="{{$customer->customer_email}}" autofocus></td>
                      </tr>
                      <tr >
                        <td ><b>{{ __('customer.customer_gender') }}</b></td>
                        <td style="padding:5px">  
                            <input name="customer_gender" value="1" type="radio"  @if ( old('customer_gender', $customer->customer_gender) == 1) checked @endif >Nam
                            <input name="customer_gender" value="2" type="radio" @if ( old('customer_gender', $customer->customer_gender) == 2) checked @endif >Nữ</td>
                      </tr>
                      <tr>
                        <td ><b>{{ __('customer.customer_phone') }}<sup class="text-danger">(*)</sup></b></td>
                        <td style="padding:5px"><input required max="100000000000" id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="customer_phone" value="{{$customer->customer_phone}}" autofocus></td>
                      </tr>
                    </table>
                    </div>
                    <div class="col">
                    <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('history.input_infomation') }}</h3>
                    <table style="margin-left:20px;">
                      <tr class="form-group">
                        <td ><b>{{ __('trans_histories.user_name') }}</b></td>
                        <td  style="padding:5px"> <input readonly id="user_name" type="text" class="form-control" name="user_name" value="{{ user_name($history->user_id) }}" autofocus></td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>{{ __('trans_histories.product_name') }}</b></td>
                        <td  style="padding:5px"><input readonly id="product_name" type="text" class="form-control" name="product_name" value="{{ product_name($history->product_id) }}" autofocus></td>
                      </tr>
                      <tr >
                        <td ><b>{{ __('trans_histories.transaction_status') }}<sup class="text-danger">(*)</sup></b></td>
                        <td style="padding:5px">
                          <input required id="transaction_status" type="radio"  name="transaction_status" value="1" autofocus>{{ __('trans_histories.trading') }}<br />
                          <input required id="transaction_status" type="radio"  name="transaction_status" value="2" autofocus>{{ __('trans_histories.successful_transaction') }}
                        
                        </td>
                      </tr>
                      <tr >
                        <td ><b>{{ __('trans_histories.deposit_amount') }}</b></td>
                        <td style="padding:5px">
                        <div class="input-group">
                          <input id="deposit_amount" type="number" class="form-control {{ $errors->has('deposit_amount') ? ' is-invalid' : '' }}" name="deposit_amount" autofocus >
                          <div class="input-group-prepend">
                            <div class="input-group-text">VND</div>
                          </div>
                        </div>
                        
                        </td>
                      </tr>
                      <tr >
                        <td ><b>{{ __('trans_histories.pay_status') }}<sup class="text-danger">(*)</sup></b></td>
                        <td style="padding:5px">
                          <input required id="pay_status" type="radio"  name="pay_status" value="1" autofocus>{{ __('trans_histories.not_paid') }}<br />
                          <input required id="pay_status" type="radio"  name="pay_status" value="2" autofocus>{{ __('trans_histories.payment_success') }}
                        </td>
                      </tr>
                      <!-- <tr>
                        <td ><b>Thời gian thanh toán</b></td>
                        <td style="padding:5px"><input id="paid_at" type="date" class="form-control {{ $errors->has('paid_at') ? ' is-invalid' : '' }}" name="paid_at" autofocus></td>
                      </tr> -->
                      <tr >
                        <td ><b>{{ __('trans_histories.paid_by') }}<sup class="text-danger">(*)</sup></b></td>
                        <td style="padding:5px">
                          <input required id="paid_by" type="radio"  name="paid_by" value="1" autofocus>{{ __('trans_histories.cash') }}
                          <input required id="paid_by" type="radio"  name="paid_by" value="2" autofocus>{{ __('trans_histories.transfer') }}
                        </td>
                      </tr>
                    </table>
                    </div>
                  </div>
                  <input hidden id="user_id" type="text" class="form-control" name="user_id" value="{{$history->user_id}}" >
                  <input hidden id="product_id" type="text" class="form-control" name="customer_id" value="{{$history->customer_id}}" >
                  <input hidden id="product_id" type="text" class="form-control" name="product_id" value="{{$history->product_id}}" >
                  <input hidden id="history_id" type="text" class="form-control" name="history_id" value="{{$history->id}}" >

                  <div style="text-align: center;"> 
                      <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                        <i class="fa fa-times" aria-hidden="true"></i>{{ __('users.cancel') }}
                      </button>
                      <button type="submit" class="btn btn-success">
                        <i class="fa fa-save" aria-hidden="true"></i>{{ __('trans_histories.agree') }}
                      </button>
                    </div>
      </form>
    </div>
  </div>
</div>

<!-- ./modal update profile -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: #ad2d31; text-align: center; font-size: 130%; font-family: 'Times New Roman'; color:white">
                    CÔNG TY TNHH MTV QUẢN LÝ BẤT ĐỘNG SẢN PHÚ CÁT
                </div>
                <div class="card-body">
                        @if (Session::has('flash-message'))
                        <div class="alert alert-danger alert-dismissible" id="success-alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Thông báo!</strong> {{Session::get('flash-message')}}
                        </div>
                        @else
                        @endif
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label text-md-right "></label>
                            <div class="col-md-6">
                                <div class="input-group mb-2">
                                    <input id="login" type="text"
                                    class="form-control{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="login" value="{{ old('username') ?: old('email') }}" placeholder="Tên đăng nhập hoặc Email" required autofocus >
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-user" aria-hidden="true"></i>   
                                        </div>
                                    </div>
                                    @if ($errors->has('username') || $errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-3 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                <div class="input-group mb-2">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Mật khẩu" required>
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-key" aria-hidden="true"></i>    
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    
                                </div>                
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <div class="form-row align-items-center">
                                    <div class="col-7">
                                        <div class="form-check mb-2">
                                            <input class="form-check-input" type="checkbox" name="remember" id="autoSizingCheck" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="autoSizingCheck">
                                                {{ __('users.remember_me') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <button type="submit" class="btn btn-danger">
                                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                                            {{ __('users.btn_login') }} 
                                        </button> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

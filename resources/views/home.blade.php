@extends('layouts.master')
@section('title')
 {{ 'Tổng quan' }}
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Bảng điều khiển</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
              <li class="breadcrumb-item active">Bảng điều khiển</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
@php
  $add_user = App\User::where('active', 0)->get();
  $add_history = App\History::where('consulting_status', 0)->get();
  $add_trans = App\TransactionHistories::where('pay_status', 1)->get();
@endphp
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
         <!-- Small Box (Stat card) -->
         <!-- <h5 class="mb-2 mt-4">Small Box</h5> -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info" style="background-color: #17a2b8!important; box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);">
              <div class="inner">
                <h3>{{ count($add_history) }}</h3>

                <p>Có {{ count($add_history) }} tư vấn mới </p>
              </div>
              <div class="icon">
                <i class="fa fa-shopping-cart"></i>
              </div>
              <a href="{{ route('lich-su-tu-van.index') }}" class="small-box-footer">
                Xem thêm <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ count($add_trans) }}</h3>

                <p>Có {{ count($add_trans) }} lịch sự  giao dịch mới</p>
              </div>
              <div class="icon">
                <i class="fa fa-money" aria-hidden="true"></i>
                <!-- <i class="ion ion-stats-bars"></i> -->
              </div>
              <a href="{{ route('lich-su-giao-dich.index') }}" class="small-box-footer">
                 Xem thêm <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ count($add_user) }}</h3>

                <p>Có {{ count($add_user) }} dùng đăng ký mới cần kích hoạt</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{ route('user.index') }}" class="small-box-footer">
                Xem thêm <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <!-- <div class="col-lg-3 col-6"> -->
            <!-- small card -->
            <!-- <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div> -->
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable">
          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@push('js')
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Morris.js charts -->
  <!-- <script src="{{ asset('plugins/morris/raphael-min.js') }}"></script> -->
  <!-- <script src="{{ asset('plugins/morris/morris.min.js') }}"></script> -->
  <!-- Sparkline -->
  <!-- <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script> -->
  <!-- jvectormap -->
  <!-- <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script> -->
  <!-- <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> -->
  <!-- jQuery Knob Chart -->
  <!-- <script src="{{ asset('plugins/knob/jquery.knob.js') }}"></script> -->
  <!-- daterangepicker -->
  <script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
  <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
  <!-- datepicker -->
  <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <!-- <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script> -->
@endpush
@extends('layouts.master')
@section('title')
 {{ __('users.users')  }}
@stop

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.user_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{ __('users.users') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.users') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div style="width:auto">
                <button type="button" data-toggle="modal" data-target="#modalCreateForm" class="btn btn-default ">{{ __('usergroup.add') }}
                </button>
                <button id="refresh" type="button" class="btn btn-default">
                  <i  class="fa fa-refresh" aria-hidden="true" data-toggle="tooltip" title="Làm mới"></i>
                </button>
              </div>
              <br />
              <table id="tableData" class="table table-bordered table-striped" >
                <thead>
                <tr>
                  <th>{{ __('usergroup.group_name') }}</th>
                  <th>{{ __('usergroup.note') }}</th>
                  <th>{{ __('usergroup.action') }}</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($usergroups as $usergroup)
                <tr>
                  <td>{{ $usergroup->group_name }}</td>
                  <td >
                    {{ $usergroup->note }}
                  </td>
                  <td>
                    <a href="javascript(0);" data-toggle="modal" data-target="#modalEditForm{{$usergroup->id}}"> 
                      <i data-toggle="tooltip" title="Chỉnh sửa" style="text-align: right;" class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>  | 
                    <a href="javascript(0);" data-toggle="modal" data-target="#confirm{{$usergroup->id}}" > 
                      <i data-toggle="tooltip" title="Xóa" style="text-align: right;" class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
<!-- Modal -->
@include('usergroup.create')
@if (empty($usergroups))
  @include('usergroup.edit')
  @include('usergroup.delete')
@elseif (isset($usergroups))
  @foreach ($usergroups as $usergroup)
    @include('usergroup.edit')
    @include('usergroup.delete')
  @endforeach
@endif
<!-- /.modal -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('ready')
<script>
  $('[data-toggle="tooltip"]').tooltip(); 
  $(
    function () {
    var table =  $("#tableData").DataTable({
      "processing": true,
      "responsive": true,
      "select": true,
      "stateSave": true,
      // "serverSide": true,
    });
    $("#refresh").on("click", function () { 
         table.ajax.reload(null, false); 
        });
  });
</script>
@endpush
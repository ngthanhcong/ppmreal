<div class="md-form mb-3">
    <label for="exagroup_namempleInputEmail1">{{ __('usergroup.group_name') }}</label>
    <input id="group_name" type="text" class="form-control{{ $errors->has('group_name') ? ' is-invalid' : '' }}" name="group_name" value="{{ old('group_name') }}" autofocus>
</div>
<div class="md-form mb-3">
    <label for="note">{{ __('usergroup.note') }}</label>
    <input id="note" type="text" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" value="{{ old('note') }}" autofocus>
</div>
    </div>
<div class="modal-footer d-flex justify-content-center">
    <button type="submit" class="btn btn-success">
        <i class="fa fa-save" aria-hidden="true"></i>
            {{ __('usergroup.save') }}
    </button>
    <button data-dismiss="modal" aria-label="Close" class="btn btn-default">
        <i class="fa fa-times" aria-hidden="true"></i>
            {{ __('usergroup.cancel') }}
    </button>
</div>
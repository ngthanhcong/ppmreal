
<div class="modal fade" id="modalEditForm{{ $usergroup->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header card-primary">
            <!-- <h4 class="modal-title w-100 font-weight-bold">Sign up</h4> -->
            <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('usergroup.edit_user') }}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body mx-3">
      <form method="post" action="{{ route('usergroup.update', $usergroup->id) }}">
        @csrf
        @method('PUT')
        @include('usergroup.form.edit')
      </div>
      </form>
    </div>
  </div>
</div>

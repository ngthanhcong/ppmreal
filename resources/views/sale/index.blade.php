@extends('layouts.master')
@section('title')
 {{ __('users.sale')  }}
@stop
<style>
 #map {
        /* height: 550px; */
        height: calc(100% - 4rem);
        margin: 0;
        padding: 0px
      }
.map-note{
  position: absolute;
  top: 66px;
  right: 11px;
}
</style>
@php

  $filters = new \stdClass();
  $filters->categories = \App\Category::get();
  $filters->cost_fields = __('product.cost-fields');
  $filters->directions = __('product.directions');
  $filters->length = __('product.acreages.length');
  $filters->width = __('product.acreages.width');
  $filters->acres = __('product.acres.acreages');

@endphp
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.sale')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.sale') }}</li>
            </ol>
          </div>
        </div>
      </div>
    </section> -->
    <!-- /.container-fluid -->
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.sale') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
				<form action="{{ route('sale.index')}}" method="get">
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                        <label for="">{{ __('sales.keyword_sale') }}</label>
                        <input type="text" class="form-control" name="keyword" placeholder="{{ __('sales.search_keyword_sale') }}">
                    </div>
                    
                  </div>
                  <div class="form-group">
                  <button type="submit" class="btn btn-outline-secondary" style="margin-top: 30px;">{{ __('sales.search_sale') }}</button>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-sm">
                      <div class="form-group">
                          <select name="category" class="form-control" id="category">
                          <option value="">{{ __('sales.category_sale') }}</option>
                          @foreach($filters->categories as $cat)
                              <option value="{{ old('$cat->id', $cat->id ) }}" {{ $cat->id == old('$cat->id') ? 'selected' : '' }}>{{ $cat->category_name }}</option>
                          @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-sm">
                      <div class="form-group">
                          <select name="acreage" class="form-control" id="acreage">
                          <option value="">{{ __('sales.acreage_sale') }}</option>
                          @foreach($filters->acres as $idx => $ac)
                              <option value="{{ old('$idx', $idx) }}">{!! $ac['min'] . ' - ' . $ac['max'] . ' m<sup>2</sup>' !!}</option>
                          @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-sm">
                      <div class="form-group">
                          <select name="price" class="form-control" id="price">
                          <option value="">{{ __('sales.price_sale') }}</option>
                          @foreach($filters->cost_fields['vi'] as $idx => $cf)
                            @if(is_array($cf))
                            <option value="{{ old('$idx', $idx) }}">
                              {{ price_to_str($cf['min'], 1, 'vi') . ' - ' . price_to_str($cf['max'], 1, 'vi') . ' (' . $filters->cost_fields['unit_' . 'vi'] .')' }}
                            </option>
                            @endif
                          @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-sm">
                      <div class="form-group">
                          <select name="direction" class="form-control" id="direction">
                          <option value="">{{ __('sales.direction_sale') }}</option>
                          @foreach (__('product.directions') as $key => $drt )
                            <option value="{{ $key }}">{{ $drt['vi'] . ' (' . $drt['en'] . ')' }}</option>
                        @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-sm">
                      <div class="form-group">
                          <select name="status" class="form-control" id="status">
                          <option value="">{{ __('sales.status_sale') }}</option>
                          <option value="1">{{ __('sales.selling_sale') }}</option>
                          <option value="2">{{ __('sales.solded_sale') }}</option>
                          <option value="3">{{ __('sales.consulting_sale') }}</option>
                          </select>
                      </div>
                    </div>
                  </div>                   
                </form>
                <div class="row">
					        <div class="col-sm">{{ __('sales.found_sale') }}
                        @if (empty($qty))
                          {{ $qty_all }}
                        @else
                          {{ $qty }}
                        @endif 
                        {{ __('sales.land_project_sale') }}
                  </div>
                  <div class="col-sm">
                    {{ __('sales.symbol_map_sale') }}
                  </div>
                  <div class="col-sm">
                    <img src='{{ asset('uploads/images/red.png') }}' style='max-width:20px;'/> {{ __('sales.land_sold_sale') }}
                  </div>
                  <div class="col-sm">
                    <img src='{{ asset('uploads/images/yellow.png') }}' style='max-width:20px;'/> {{ __('sales.land_consulted_sale') }}
                  </div>
                  <div class="col-sm">
                    <img src='{{ asset('uploads/images/green.png') }}' style='max-width:20px;'/> {{ __('sales.land_selling_sale') }}
                  </div>
                  <div class="col-sm">
                    <img src='{{ asset('uploads/images/map-marker-icon.png') }}' style='color:red; max-width:20px;'/> {{ __('sales.land_position_sale') }}
                  </div>
				          </div>
                <!-- map -->
                <div class="row">
                    <div id="map" class="col-sm"></div>
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@endsection

@push('js')
<script>
function initialize() {
    var center;
    var markers = new Array();
    var locations = new Array();
    var lat_arr = new Array();
    var lng_arr = new Array();
    var areas = new Array();
    var contents = new Array();
    @foreach ($products as $product)
      lat_arr.push({{ $product->lat }});
      lng_arr.push({{ $product->lng }});
      locations.push({lat: {{ $product->lat }}, lng: {{ $product->lng }} });
    @endforeach
    // console.log(locations);
    if (lat_arr.length == 0 && lng_arr.length == 0)
    {
      center = {lat: 9.9650508, lng: 105.5096585 };
    }
    else
      center = {lat: Math.max(...lat_arr), lng: Math.max(...lng_arr)};
      // Add some markers to the map.
      // Note: The code uses the JavaScript Array.prototype.map() method to
      // create an array of markers based on a given "locations" array.
      // The map() method here has nothing to do with the Google Maps API.
    // var markerClusterer = locations.map(function(location, i) {
    //     return new google.maps.Marker({
    //       position: location,
    //       //  label: labels[i % labels.length]
    //     });
    //   });
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: center,
    mapTypeId: 'terrain'
    });
    
    // add products to array
    @foreach ($products as $product)
        areas = [{{ $product->area }}];
        markers.push([ '{!! $product->product_name !!}' ,  {{ ( $product->lat) }} ,  {{( $product->lng ) }}  ]);
        @php
          $histories = App\History::where('user_id', \Auth::id())->where('consulting_status', 0)->first();
        @endphp
        content = [
          '<form method="post" action="{{ route('sale.store')}}" style="width:360px; font-size:18px;">'+
          '<input type="hidden" name="_method" value="POST">'+
          '<input type="hidden" name="_token" value="{{ csrf_token() }}">'+
          '<input type="hidden" name="product_id" value="{{ $product->id }}">'+
          '<label style="font-weight: bold;">{!! $product->product_name !!}</label><br/>' +
          '<label style="font-weight: bold;">{{ __('product.price') }}:</label> '+
          '@if (isset($product->price))'+
          '{{ number_format($product->price, 0) }} VND '+
          '@else'+
          '<span class="badge bg-success">'+
          'Thỏa thuận'+
          '</span>'+
          '@endif'+
          '</label>'+
          '<br/>'+
          '<label style="font-weight: bold;">{{ __('product.acreage') }}: </label> {{ $product->acreage }} m<sup>2</sup><br/>'+
          "<label style='font-weight: bold;'>{{ __('product.direction') }}: </label> @foreach (__('product.directions') as $key => $drt ) @if ($key == $product->direction) {{ $drt['vi'] . ' (' . $drt['en'] . ')' }} @endif @endforeach <br />"+
          '@if ($product->status == 1 )'+
          '<div class="form-row align-items-center">'+
            '<div class="col-8">'+
              '<label class="sr-only" for="inlineFormInput">Customer Name</label>'+
              '@if (isset($histories))'+
                "<span class='badge bg-info'>{{ __('Bạn chỉ có tư vấn cho một khách hàng!') }}</span>"+
                '@else'+
                '<input required type="text" class="form-control mb-2" id="customer_name" name="customer_name" placeholder="{{ __('sales.input_customer_name') }}" required>'+
                  '<div class="col-auto">'+
                '<button type="submit" class="btn btn-primary mb-2">{{ __('sales.consulted') }}</button>'+
              '</div>'+
              '@endif'+
            '</div>'+
          '</div>'+
          '@elseif ($product->status == 3)'+
          '<div class="from-group">'+
            '<span class="badge bg-info">'+
              '{{ __('sales.status_consulted') }}'+
            '</span>'+
          '</div>'+
          '@elseif ($product->status == 2)'+
          '<div class="from-group">'+
            '<span class="badge bg-info">'+
              '{{ __('sales.status_traded') }}'+
              '</span>'+
          '</div>'+
          '@endif'+
          '<label style="font-weight: bold;">'+
            '<a style="font-size:14px;" href="{{ route('sale.show', $product->alias) }}">{{ __('sales.detail_view') }}</a>'+
          '</label><br />'+
          '@if ($product->status == 3)'+
          '<label style="font-weight: bold;">'+
            '<a style="font-size:14px;" href="{{ route('lich-su-tu-van.index') }}">{{ __('sales.review_before_closing') }}</a>'+
          '</label>'+
          '@else'+
          '@endif'+
          '</form>'
        ],
        contents.push(content);
        // set color status area
        @if ($product->status == 1)
            var strokeColor = "#008000";
            var fillColor = "#008000";
        @elseif ($product->status == 3)
            var strokeColor = "#FFFF00";
            var fillColor = "	#FFFF00";
        @elseif ($product->status == 2)
            var strokeColor = "#FF0000";
            var fillColor = "#FF0000";
        @endif
        var acreages = new google.maps.Polygon({
            paths: areas,
            strokeColor: strokeColor,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: fillColor,
            fillOpacity: 0.35
      });
      acreages.setMap(map);
    @endforeach
    // Add a marker clusterer to manage the markers.
    // var markerCluster = new MarkerClusterer(map, markerClusterer,
    // {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    var infowindow =  new google.maps.InfoWindow({maxWidth: 360});
    var marker, count;
    console.log();
    for (count = 0; count < markers.length; count++) {
        marker = new google.maps.Marker({
        position: new google.maps.LatLng(markers[count][1], markers[count][2]),
        map: map,
        title: markers[count][0]
      });
      google.maps.event.addListener(marker, 'click', (function (marker, count) {
        return function () {
          infowindow.setContent(contents[count][0]);
          infowindow.open(map, marker);
      }
      })(marker, count));
    }
}
</script>

<script 
src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-uDYSJbafK6Ptn_JaehA5avxHSPxkcNI&callback=initialize"
    async defer></script>
@endpush

<style>
 #map{
        height: 450px;
        margin: 0;
        padding: 0px
      }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-uDYSJbafK6Ptn_JaehA5avxHSPxkcNI&libraries=drawing&callback=initialize"
></script>
<script>
function initialize() {
    var center = {lat: 9.9650508, lng: 105.5096585 };
    var markers = new Array();
    var locations = new Array();
    var areas = new Array();
    var contents = new Array();
    locations = [
        ['Philz Coffee 1', 34.046438, -118.259653],
        ['Philz Coffee 2', 34.017951, -118.493567],
        ['Philz Coffee 3', 34.143073, -118.132040],
        ['Philz Coffee 4', 33.655199, -117.998640],
        ['Philz Coffee 5', 34.142823, -118.254569]
    ];
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: center
    });
    var infowindow =  new google.maps.InfoWindow({});
    content = [
        ['Philz Coffee 1<br>\
        801 S Hope St A, Los Angeles, CA 90017<br>\
        <a href="https://goo.gl/maps/L8ETMBt7cRA2">Get Directions</a>'],
        ['Philz Coffee 2<br>\
        525 Santa Monica Blvd, Santa Monica, CA 90401<br>\
        <a href="https://goo.gl/maps/PY1abQhuW9C2">Get Directions</a>'],
        ['Philz Coffee 3<br>\
        146 South Lake Avenue #106, At Shoppers Lane, Pasadena, CA 91101<br>\
        <a href="https://goo.gl/maps/eUmyNuMyYNN2">Get Directions</a>'],
        ['Philz Coffee 4<br>\
        21016 Pacific Coast Hwy, Huntington Beach, CA 92648<br>\
        <a href="https://goo.gl/maps/Cp2TZoeGCXw">Get Directions</a>'],
        ['Philz Coffee 5<br>\
        252 S Brand Blvd, Glendale, CA 91204<br>\
        <a href="https://goo.gl/maps/WDr2ef3ccVz">Get Directions</a>']
    ];
    var marker, count;
    for (count = 0; count < locations.length; count++) {
        marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[count][1], locations[count][2]),
        map: map,
        // title: locations[count][0]
    });
    google.maps.event.addListener(marker, 'click', (function (marker, count) {
      return function () {
        infowindow.setContent(content[count][0]);
        infowindow.open(map, marker);
    }
    })(marker, count));
  }
    console.log(locations)
    console.log(content)
}



google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map"></div>
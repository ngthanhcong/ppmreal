  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" id="collapse_menu"><i class="fa fa-bars"></i></a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" style="cursor: auto;">
            <span>{{ Auth::user()->note }}</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <!-- Authentication Links -->
        @guest
        @else
        <a class="nav-link" data-toggle="dropdown" href="#">
		      <i class="fa fa-user-o" aria-hidden="true"></i>
          <span>{{ Auth::user()->fullname}}</span>
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="text-align:left">
          <span class="dropdown-item dropdown-header">
            <a href="{{ route('home') }}" class="dropdown-item dropdown-header">
              <i class="nav-icon fa fa-dashboard"></i>
              <span>
                Tổng quan
              </span>
            </a>
          </span>
          <span class="dropdown-item dropdown-header">
            <a href="{{ route('profile') }}" class="dropdown-item dropdown-header">
              <i class="fa fa-user-circle-o" aria-hidden="true"></i>
              {{ __('users.profile')}}
            </a>
          </span>
          <div class="dropdown-divider"></div>
          <span class="dropdown-item dropdown-header">
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" 
                class="dropdown-item dropdown-header" style="text-align:left">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
                {{ __('users.logout')}}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </span>
          
        </div>
        @endguest
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li> -->
    </ul>
  </nav>
  <!-- /.navbar -->
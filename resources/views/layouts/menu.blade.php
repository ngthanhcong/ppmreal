  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('dist/img/logo1.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ __('PPM Group') }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
      @if (Auth::user()->hasRole('Admin'))
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <!-- The Current User Can Update The Post -->
          <li class="nav-header ">{{ __('users.management') }}</li>
          <li class="nav-item">
             <a href="{{ route('sale.index') }}" class="nav-link {{ Request::is('sale*') ? 'active' : '' }}">
                  <i class="fa fa-home nav-icon"></i>
                  <p>{{ __('users.category_sale')}}</p>
                </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('lich-su-tu-van.index') }}" class="nav-link {{ Request::is('lich-su-tu-van*') ? 'active' : '' }}" >
              <i class="fa fa-list-alt nav-icon"></i>
              <p>{{ __('users.histories_advisory')}}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('lich-su-giao-dich.index') }}" class="nav-link {{ Request::is('lich-su-giao-dich*') ? 'active' : '' }}" >
              <i class="fa fa-list-alt nav-icon"></i>
              <p>{{ __('users.histories_transaction')}}</p>
            </a>
          </li>
          <li class="nav-header">
            {{ __('users.user_management') }}
          </li>
          <li class="nav-item">
            <a href="{{ route('role.index') }}" class="nav-link {{ Request::is('role*') ? 'active' : '' }}">
                <i class="fa fa-users nav-icon"></i>
                <p>{{ __('users.role') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('permission.index') }}" class="nav-link {{ Request::is('permission*') ? 'active' : '' }}">
                <i class="fa fa-lock nav-icon"></i>
                <p>{{ __('users.permission') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('user.index') }}" class="nav-link {{ Request::is('user*') ? 'active' : '' }}">
                <i class="fa fa fa-user nav-icon"></i>
                <p>{{ __('users.user') }}</p>
            </a>
          </li>
          <li class="nav-header ">{{ __('users.real_estate_management') }}</li>
          <li class="nav-item">
          <a href="{{ route('product.index') }}" class="nav-link {{ Request::is('product*') ? 'active' : '' }}">
                  <i class="fa fa-home nav-icon"></i>
                  <p>{{ __('users.product')}}</p>
                </a>
          </li>
          <li class="nav-item">
          <a href="{{ route('category.index') }}" class="nav-link {{ Request::is('category*') ? 'active' : '' }}" >
                  <i class="fa fa-list-alt nav-icon"></i>
                  <p>{{ __('users.category')}}</p>
                </a>
          </li>          
        </ul>
        @elseif (Auth::user()->hasRole('Employees'))
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-header ">{{ __('users.management') }}</li>
          <li class="nav-item">
             <a href="{{ route('sale.index') }}" class="nav-link {{ Request::is('sale*') ? 'active' : '' }}">
                  <i class="fa fa-home nav-icon"></i>
                  <p>{{ __('users.category_sale')}}</p>
                </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('lich-su-tu-van.index') }}" class="nav-link {{ Request::is('lich-su-tu-van*') ? 'active' : '' }}" >
              <i class="fa fa-history" aria-hidden="true"></i>
              <p>{{ __('users.histories_advisory')}}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('lich-su-giao-dich.index') }}" class="nav-link {{ Request::is('lich-su-giao-dich*') ? 'active' : '' }}" >
              <i class="fa fa-history" aria-hidden="true"></i>
              <p>{{ __('users.histories_transaction')}}</p>
            </a>
          </li>
          @can('create-user-sale')
          <li class="nav-item">
            <a href="{{ route('user-sale.index') }}" class="nav-link {{ Request::is('user-sale*') ? 'active' : '' }}" >
              <i class="fa fa-users" aria-hidden="true"></i>
              <p>{{ __('users.staff')}}</p>
            </a>
          </li>
          @endcan
        </ul>
        @endif
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="{{ asset('dist/img/logo.ico') }}">
  <title>
    @hasSection('title')
      @yield('title')
    @else
      Not found
    @endif    
  </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="{{ asset('plugins/iCheck/flat/blue.css') }}"> -->
  <!-- Morris chart -->
  <!-- <link rel="stylesheet" href="{{ asset('plugins/morris/morris.css') }}"> -->
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}"> -->
  <!-- Date Picker -->
  <!-- <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}"> -->
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Time+New+Roman:300,400,400i,700" rel="stylesheet"> -->
  <!-- Toastr -->
  <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
  <!-- Responsive Datatable Bootstrap 4 -->
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
  <!-- daterangepicker -->
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}" />

  @stack('css')
</head>
<body class="hold-transition sidebar-mini">
  

    <div class="wrapper">
        @include('layouts.menu')
        @include('layouts.navbar')
        <!-- <div id="timer" style="font-size: 20px; margin-left:270px">
          Bạn còn 
          <input readonly id="minutes" type="text" style="width: 35px; border: none; background-color:none; font-size: 25px; font-weight: bold;"> :
          <input readonly id="seconds" type="text" style="width: 35px; border: none; background-color:none; font-size: 25px; font-weight: bold;"> để tư vấn.
        </div> -->
        @php
          $histories = App\History::where('user_id', \Auth::id())->where('consulting_status', 0)->first();
          $new_user = App\User::where('active', 0)->get();
          $new_trans = App\TransactionHistories::where('pay_status', 1)->get();
        @endphp
        @if (isset($histories))
          <marquee style="font-size:20px; color: red" scrollamount="5"> {{ __('sales.infomation_sale') }}</marquee>
          
        @endif
        @if (count($new_user) != 0)
          <marquee style="font-size:20px; color: red" scrollamount="5"> {{ __('sales.new_user') }}</marquee>
        @else
        @endif
        @if (count($new_trans) != 0)
          <marquee style="font-size:20px; color: red" scrollamount="5"> {{ __('users.new_trans') }}</marquee>
        @else
        @endif
        @yield('content')
        <!-- Control sidebar content goes here -->
        <aside>
        </aside>
    </div>
  <!-- ./wrapper -->
  <!-- jQuery -->
  <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- SlimScroll -->
  <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <!-- <script src="{{ asset('dist/js/demo.js') }}"></script> -->
  <!-- Toastr -->
  <script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
  <!-- Moment -->
  <script type="text/javascript" src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
@if(Session::has('message'))
  <script>
		var type="{{Session::get('alert-type','info')}}"
		switch(type){
			case 'info':
		    toastr.info("{{ Session::get('message') }}");
		    break;
	    case 'success':
	      toastr.success("{{ Session::get('message') }}");
	      break;
      case 'warning':
	      toastr.warning("{{ Session::get('message') }}");
	      break;
	    case 'error':
		    toastr.error("{{ Session::get('message') }}");
		    break;
		}
  </script>
@endif
<!-- Javascript -->
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<!-- String to alias -->
<script type="text/javascript">
    function str_to_alias(id, idResult) {
    var text, slug;
    text = $('#' + id).val();
    slug = text.toLowerCase();
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    slug = slug.replace(/ /gi, "-");
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    $('#' + idResult).val(slug);
};
</script>
<!-- CKEditor -->
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<!--Jquery Responsive Datatable Bootstrap 4 -->
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- Load img before update -->
<script type="text/javascript">
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#avatar_show').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        }
  }
</script>
<script type="text/javascript">
    function autoRefreshPage()
    {
        window.location.href = '{{ route('lich-su-tu-van.index')}}';
    }
    // autoRefreshPage sau 10 phút 
    setInterval('autoRefreshPage()', 600000);
</script>
<!-- count time
<script type="text/javascript">
// set minutes
var mins = 45;
 
// calculate the seconds (don't change this! unless time progresses at a different speed for you...)
var secs = mins * 60;
function countdown() {
	setTimeout('Decrement()',1000);
}
function Decrement() {
	if (document.getElementById) {
		minutes = document.getElementById("minutes");
		seconds = document.getElementById("seconds");
		// if less than a minute remaining
		if (seconds < 59) {
			seconds.value = secs;
		} else {
			minutes.value = getminutes();
			seconds.value = getseconds();
		}
		secs--;
		setTimeout('Decrement()',1000);
	}
}
function getminutes() {
	// minutes is seconds divided by 60, rounded down
	mins = Math.floor(secs / 60);
	return mins;
}
function getseconds() {
	// take mins remaining (as seconds) away from total seconds remaining
	return secs-Math.round(mins *60);
}
</script>
<script>
countdown();
</script> -->
<!-- CKEDITOR -->
<script defer>
if($("textarea").length > 0){
  CKEDITOR.replace( 'content', {
        filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}',
   });
}
</script>

<!--Ready-->
<script>
  //tooltip
  $('[data-toggle="tooltip"]').tooltip(); 
   //Initialize Select2 Elements
  $('.select2').select2();
  // myPopover
  $("#myPopover").popover({
        title: "",
        content: "",
        html: true
  }); 
  <!--DataTable-->
  $(
    function () {
      var table =  $("#tableData").DataTable({
        "responsive": true,
        // "processing": true,
        // "serverSide": true,
        // "ajax": "scripts/server_processing.php"
    });
  });
  $('input[name="birthday"]').daterangepicker({
        "autoApply": true,
        "drops": "up",
        "singleDatePicker": true,
        "showDropdowns": true,
        "minYear": 1901,
        "maxYear": parseInt(moment().subtract(19, "years").format("YYYY"), 19),
        "maxDate": moment().date(31).month(12).subtract(10, "years"),
        "startDate": moment().hour(1).minute(0).subtract(10, "years"),
        "locale": {
            "format": "DD/MM/YYYY"
        }
    });
    $('#birthday').on('apply.daterangepicker', function(ev, picker) {
      $('#birthday').val(picker.startDate.format('DD/MM/YYYY')); 
    });
</script>

<script>
  $( document ).ready(function() {
    $("#collapse_menu").click();
  });
</script>
<!-- ./Ready -->
@stack('js')
@stack('ready')
</body>
</html>


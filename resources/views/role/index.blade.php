@extends('layouts.master')
@section('title')
 {{ __('users.role')  }}
@stop

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.user_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.role') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.role') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div style="width:auto">
                <button type="button" data-toggle="modal" data-target="#modalCreateForm" type="button" class="btn btn-outline-secondary ">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('role.role_create') }}
                </button>
                <!-- <button type="button" class="btn btn-outline-secondary ">
                  <i class="fa fa-refresh" aria-hidden="true" data-toggle="tooltip" title="Làm mới"></i>
                </button> -->
              </div>
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>{{ __('role.role_name') }}</th>
                  <th>{{ __('role.note') }}</th>
                  <th>{{ __('role.created_at') }}</th>
                  <th>{{ __('role.updated_at') }}</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody>
                  @foreach ($role as $role)
                <tr>
                  <td>{{ $role->slug }}</td>
                  <td>{{ $role->name }}</td>
                  <td>{{ date('d/m/Y H:i:s', strtotime($role->created_at)) }}</td>
                  <td>{{ date('d/m/Y H:i:s', strtotime($role->updated_at)) }}</td>
                  <td>
                    <a href="{{ route('role.edit',$role->id) }}"> 
                      <i data-toggle="tooltip" title="Chỉnh sửa" style="text-align: right;" class="	fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>  
                    | 
                    <a href="javascript(0);" data-toggle="modal" data-target="#confirm{{$role->id}}" > 
                      <i data-toggle="tooltip" style="color:red " title="Xóa" style="text-align: right;" class="fa fa-trash-o" aria-hidden="true" ></i>
                    </a>
                  </td>
                </tr>
                <div class="modal fade" id="confirm{{ $role->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                  aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body " style="background-image: url({{ asset('dist/img/delete.jpg') }}); background-repeat: no-repeat; background-size: 130px; padding-left: 130px;">
                        <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('role.role_del') }}</h3>
                        <h6> {{ __('Nhóm') }}: {{ $role->name }}</h6>
                        <form method="post" action="{{ route('role.destroy', $role->id) }}">
                            @csrf
                            @method('DELETE')
                            <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                            <i class="fa fa-times" aria-hidden="true"></i>
                                {{ __('role.cancel') }}
                            </button>
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                                    {{ __('role.delete') }}
                            </button>
                        </form>
                        </div>
                    </div>
                  </div>
                </div>
                @endforeach
                </tbody>
              </table>
              <!-- Modal Create-->
              
              <!-- /.modal -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@include('role.create')

<!-- /.modal -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

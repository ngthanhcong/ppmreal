@extends('layouts.master')
@section('title')
 {{ __('users.role')  }}
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.role')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.role') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
             <h3 class="card-title">{{ __('role.edit_role') }}: {{$role->name}}</h3>
            </div>  
            <!-- /.card-header -->
            <div class="card-body">
                <form method="post" action="{{ route('role.update', $role->id) }}">
                  @csrf
                  @method('PUT')
                  <div class="form-group" style="width:auto; margin-top: -15px; margin-bottom: -10px;" >
                    <button type="button" class="btn btn-outline-secondary">
                        <a style="text-decoration: none; color: black;" href="{{ route('role.index') }}">
                          <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('users.back') }}
                        </a>
                    </button>
                  </div>
                  <hr />
                  <div class="container col-md-6">
                    <div class="form-group ">
                      <label for="slug">{{ __('role.role_name') }}</label>
                      <input readonly type="text" class="form-control" id="slug" name="slug" value="{{ old('slug', $role->slug) }}">
                    </div>
                    <div class="form-group">
                      <label for="name">{{ __('role.note') }}</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $role->name)}}">
                    </div>
                  </div> 
                  <div style="text-align: center;"> 
                      <button type="reset" class="btn btn-outline-secondary">
                        <i class="fa fa-times" aria-hidden="true"></i>{{ __('users.cancel') }}
                      </button>
                      <button type="submit" class="btn btn-success">
                        <i class="fa fa-save" aria-hidden="true"></i> {{ __('users.save') }}
                      </button>
                  </div> 
                </form>
            </div>
            <!-- /.card-body -->
        </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@endpush

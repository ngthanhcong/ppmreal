<div class="modal fade" id="modalCreateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header card-primary">
            <!-- <h4 class="modal-title w-100 font-weight-bold">Sign up</h4> -->
            <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('role.role_create') }}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body mx-3">
      <form method="post" action="{{ route('role.store') }}">
        @csrf
        @method('POST')
        <div class="md-form mb-3">
            <label for="slug">{{ __('role.role_name') }}</label>
            <input id="slug" type="text" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" value="{{ old('slug') }}" autofocus>
        </div>
        <div class="md-form mb-3">
            <label for="name">{{ __('role.note') }}</label>
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" autofocus>
        </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="submit" class="btn btn-success">
          <i class="fa fa-save" aria-hidden="true"></i>
          {{ __('users.save') }}
        </button>
        <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
            <i class="fa fa-times" aria-hidden="true"></i>
            {{ __('users.cancel') }}
        </button>
      </div>
      </form>
    </div>
  </div>
</div>

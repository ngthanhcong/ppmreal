@extends('layouts.master')
@section('title')
 {{ __('Thông tin chi tiết')  }}
@stop

@php
    function user_name($user_id)
    {
        $user_name = \App\User::where('id', $user_id)->first();
        return $user_name->fullname;
    }
    function customer_name($customer_id)
    {
        $customer_name = \App\Customer::where('id', $customer_id)->first();
        return $customer_name->customer_name;
    }
    function product_name($product_id)
    {
        $product_name = \App\Product::where('id', $product_id)->first();
        return $product_name->product_name;
    }
    function customer($customer_id)
    {
        $customer = \App\Customer::where('id', $customer_id)->first();
        return $customer;
    }
    function product($product_id)
    {
        $product = \App\Product::where('id', $product_id)->first();
        return $product;
    }
    function get_address($ward_id)
    {
      $ward = \App\Ward::where('id', $ward_id)->first();
      $district = \App\District::where('id', $ward->district_id)->first();
      $town = \App\Town::where('id', $district->town_id)->first();
      $address = $ward->ward_name .', '. $district->district_name .', '. $town->town_name ;
      return $address;
    }
    function get_category($category_id)
	  {
      $category = \App\Category::where('id', $category_id)->first();
      return $category->category_name;
	  }
@endphp
@php
 $customer = customer($trans_histories->customer_id);
 $product = product($trans_histories->product_id);
@endphp
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.histories_management')}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('Giao dịch') }}</li>
            </ol>
          </div> -->
        </div> 
      </div>
      <!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
             <h3 class="card-title">{{ __('trans_histories.details') }}</h3>
            </div>  
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group" style="width:auto; margin-top: -15px; margin-bottom: -10px;" >
                <button type="button" class="btn btn-outline-secondary">
                    <a style="text-decoration: none; color: black;" href="{{ route('lich-su-giao-dich.index') }}">
                      <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('users.back') }}
                    </a>
                </button>
              </div>
              <hr />   
              <div class="row">
                <div class="col-sm">
                  <h4>{{ __('trans_histories.trans_information')}}</h4>
                  <table style="margin-left:20px;">
                    <!-- <tbody> -->
                      <tr class="form-group">
                        <th>{{ __('trans_histories.user_name') }}:</th>
                        <td>{{ user_name($trans_histories->user_id) }}</td>
                      </tr>
                      <tr class="form-group">
                        <th>{{ __('trans_histories.customer_name') }}:</th>
                        <td>{{ customer_name($trans_histories->customer_id) }}</td>
                      </tr>
                      <tr class="form-group">
                        <th>{{ __('trans_histories.product_name') }}:</th>
                        <td>{{ product_name($trans_histories->product_id) }}</td>
                      </tr>
                      <tr class="form-group">
                        <th>{{ __('trans_histories.transaction_status') }}:</th>
                        <td style="font-size:18px">
                        @if ($trans_histories->transaction_status == 1)
                          <span class="badge bg-info">{{ __('trans_histories.trading') }}</span>
                        @elseif ($trans_histories->transaction_status == 2)
                          <span class="badge bg-success">{{ __('trans_histories.successful_transaction') }}</span>
                        @endif
                        </td>
                      </tr>
                      <tr class="form-group">
                        <th>{{ __('trans_histories.transactioned_at') }}:</th>
                        <td>{{ date('d/m/Y H:i:s', strtotime($trans_histories->transactioned_at)) }}</td>
                        </tr>
                      <tr class="form-group">
                        <th>{{ __('trans_histories.deposit_amount') }}:</th>
                        <td>
                        @if ($trans_histories->deposit_amount != null)
                          {{ number_format($trans_histories->deposit_amount) }} VND
                        @else
                          {{ __('trans_histories.no_deposit') }}
                        @endif 
                        </td>
                      </tr>
                      <tr class="form-group">
                        <th>{{ __('trans_histories.pay_status') }}:</th>
                        <td style="font-size:18px">
                        @if ($trans_histories->pay_status == 1)
                          <span class="badge bg-info">{{ __('trans_histories.not_paid') }}</span>
                        @elseif ($trans_histories->pay_status == 2)
                          <span class="badge bg-success">{{ __('trans_histories.payment_success') }}</span>
                        @endif
                        </td>
                      </tr>
                      <tr class="form-group">
                        <th>{{ __('trans_histories.paid_at') }}:</th>
                        <td>{{ date('d/m/Y H:i:s', strtotime($trans_histories->paid_at)) }}</td>
                        </tr>
                        <tr class="form-group">
                        <th>{{ __('trans_histories.paid_by') }}:</th>
                        <td style="font-size:18px">
                        @if ($trans_histories->paid_by == 1)
                          <span class="badge bg-info">{{ __('trans_histories.cash') }}</span>
                        @elseif ($trans_histories->paid_by == 2)
                          <span class="badge bg-success">{{ __('trans_histories.transfer') }}</span>
                        @endif
                        </td>
                      </tr>
                      <tr class="form-group">
                        <th>{{ __('trans_histories.note') }}:</th>
                        <td>{{ $trans_histories->note }}</td>
                        </tr>
                      <tr class="form-group">
                      </tr>
                  </table>
                  @if (($trans_histories->pay_status == 1))
                    <div class="form-group" >
                          <!-- <td> -->
                            <button
                                type="button" data-toggle="modal" data-target="#confirm"  class="btn btn-success ">
                                <i class="fa fa-money" aria-hidden="true"></i> {{ __('trans_histories.confirm_successful_transfer') }}
                            </button>
                          <!-- </td> -->
                    </div>
                  @endif
                </div>

                <div class="col-sm">
                  <h4>{{ __('trans_histories.customer_information')}}</h4>
                    <table style="margin-left:20px;">
                      <tr class="form-group">
                        <td ><b>Họ và tên</b></td>
                        <td style="padding:5px">
                        @if ($customer->customer_name != null)
                          {{$customer->customer_name}}
                        @else
                          {{ __('trans_histories.not_provided') }}
                        @endif
                        </td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>Địa chỉ</b></td>
                        <td style="padding:5px">
                        @if ($customer->customer_address != null)
                          {{$customer->customer_address}}
                        @else
                          {{ __('trans_histories.not_provided') }}
                        @endif
                        </td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>Email</b></td>
                        <td style="padding:5px">
                        @if ($customer->customer_email != null)
                          {{$customer->customer_email}}
                        @else
                          {{ __('trans_histories.not_provided') }}
                        @endif
                        </td>
                      </tr>
                      <tr >
                        <td ><b>Giới tính</b></td>
                        <td style="padding:5px">
                        @if ($customer->customer_gender == 1)
                          Nam
                        @elseif ($customer->customer_gender == 2)
                          Nữ
                        @else
                          {{ __('trans_histories.not_provided') }}
                        @endif
                        </td>
                      </tr>
                      <tr>
                        <td ><b>Số điện thoại</b></td>
                        <td style="padding:5px">
                        @if ($customer->customer_phone != null)
                          {{$customer->customer_phone}}
                        @else
                          {{ __('trans_histories.not_provided') }}
                        @endif
                        </td>
                      </tr>
                    </table>
                </div>

                <div class="col-sm">
                  <h4>{{ __('trans_histories.product_information')}}</h4>
                  <table style="margin-left:20px;">
                      <tr class="form-group">
                        <td ><b>{{ __('product.product_name') }}</b></td>
                        <td style="padding:5px">
                        {{ $product->product_name }}
                        </td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>{{ __('product.category_id') }}</b></td>
                        <td style="padding:5px">
                        {{ get_category($product->category_id) }}
                        </td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>{{ __('product.ward_id') }}</b></td>
                        <td style="padding:5px">
                        {{ get_address($product->ward_id) }}
                        </td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>{{ __('product.acreage') }}</b></td>
                        <td style="padding:5px">
                        {{ $product->acreage }} m<sup>2</sup>
                        </td>
                      </tr>
                      <tr class="form-group">
                        <td ><b>{{ __('product.price') }}</b></td>
                        <td style="padding:5px">
                        @if (isset($product->price))
                          {{ number_format($product->price, 0) }} VND 
                        @else
                          <span class="badge bg-success">
                              {{ __('Thỏa thuận') }}
                          </span>
                        @endif
                        </td>
                      </tr>
                      <tr >
                        <td ><b>{{ __('product.direction') }}</b></td>
                        <td style="padding:5px">
                        @foreach (__('product.directions') as $key => $drt )
                          @if ($key == $product->direction)
                            {{ $drt['vi'] . ' (' . $drt['en'] . ')' }}
                            @endif
                        @endforeach
                        </td>
                      </tr>
                    </table>
                </div>

              </div>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body " style="background-image: url({{ asset('dist/img/icon-thanh-toan.png') }}); background-repeat: no-repeat; background-size: 95px;margin-left: 15px; margin-top: 5px; padding-left: 110px; padding-top: 0px;">
        <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('trans_histories.confirm_transaction') }}</h3>
        <form method="post" action="{{ route('lich-su-giao-dich.update', $trans_histories->id) }}">
            @csrf
            @method('PUT')
            <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
            <i class="fa fa-times" aria-hidden="true"></i>
                {{ __('users.cancel') }}
            </button>
            <button type="submit" class="btn btn-success">
              <i class="fa fa-check" aria-hidden="true"></i>
                    {{ __('trans_histories.agree') }}
            </button>
        </form>
        </div>
    </div>
  </div>
</div>

@endsection
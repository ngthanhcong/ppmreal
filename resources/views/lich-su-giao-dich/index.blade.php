@extends('layouts.master')
@section('title')
 {{ __('Lịch sử giao dịch')  }}
@stop
@php
    function user_name($user_id)
    {
        $user_name = \App\User::where('id', $user_id)->first();
        return $user_name->fullname;
    }
    function customer_name($customer_id)
    {
        $customer_name = \App\Customer::where('id', $customer_id)->first();
        return $customer_name->customer_name;
    }
    function product_name($product_id)
    {
        $product_name = \App\Product::where('id', $product_id)->first();
        return $product_name->product_name;
    }
    function customer($customer_id)
    {
        $customer = \App\Customer::where('id', $customer_id)->first();
        return $customer;
    }      
@endphp
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.histories_management')}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.histories_transaction') }}</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.histories_transaction') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>{{ __('trans_histories.user_name') }}</th>
                  <th>{{ __('trans_histories.customer_name') }}</th>
                  <th style="width:120px">{{ __('trans_histories.product_name') }}</th>
                  <th>{{ __('trans_histories.transaction_status') }}</th>
                  <th>{{ __('trans_histories.transactioned_at') }}</th>
                  <th>{{ __('trans_histories.deposit_amount') }}</th>
                  <th>{{ __('trans_histories.pay_status') }}</th>
                  <th>{{ __('trans_histories.paid_at') }}</th>
                  <th>{{ __('trans_histories.paid_by') }}</th>
                  <td>{{ __('trans_histories.note') }}</td>
                </tr>
                </thead>
                <tbody>
                @foreach ($transaction_histories as $trans_historie)
                <tr>
                  <td>{{ user_name($trans_historie->user_id) }}</td>
                  <td>{{ customer_name($trans_historie->customer_id) }}</td>
                  <td  style="width:120px; ">
                    <a style="color:black" data-toggle="tooltip" title="{{ __('trans_histories.view') }}" href="{{ route('lich-su-giao-dich.show', $trans_historie->id ) }}"> 
                      {{ product_name($trans_historie->product_id) }}
                    </a>
                  </td>
                  <td style="font-size:18px">
                  @if ($trans_historie->transaction_status == 1)
                    <span class="badge bg-info"  data-toggle="tooltip" title="{{ __('trans_histories.confirm') }}">
                    <a style="text-decoration:none; cursor: pointer;" data-toggle="modal" data-target="#confirm">
                    {{ __('trans_histories.trading') }}
                    </a>
                    </span>
                      <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-body " style="background-image: url({{ asset('dist/img/icon-thanh-toan.png') }}); background-repeat: no-repeat; background-size: 95px;margin-left: 15px; margin-top: 5px; padding-left: 110px; padding-top: 0px;">
                              <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('trans_histories.confirm_transaction') }}</h3>
                              <form method="post" action="{{ route('lich-su-giao-dich.update', $trans_historie->id) }}">
                                  @csrf
                                  @method('PUT')
                                  <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                                  <i class="fa fa-times" aria-hidden="true"></i>
                                      {{ __('users.cancel') }}
                                  </button>
                                  <button type="submit" class="btn btn-success">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                          {{ __('trans_histories.agree') }}
                                  </button>
                              </form>
                              </div>
                          </div>
                        </div>
                      </div>
                  @elseif ($trans_historie->transaction_status == 2)
                    <span class="badge bg-success">{{ __('trans_histories.successful_transaction') }}</span>
                  @endif
                  </td>
                  <td>{{ date('d/m/Y H:i:s', strtotime($trans_historie->transactioned_at)) }}</td>
                  <td>{{  number_format($trans_historie->deposit_amount, 0) }} VND</td>
                  <td style="font-size:18px">
                  @if ($trans_historie->pay_status == 1)
                    <span class="badge bg-info">
                      {{ __('trans_histories.not_paid') }}
                    </span>
                  @elseif ($trans_historie->pay_status == 2)
                    <span class="badge bg-success">{{ __('trans_histories.payment_success') }}</span>
                  @endif
                  </td>
                  <td>{{ date('d/m/Y H:i:s', strtotime($trans_historie->paid_at)) }}</td>
                  <td style="font-size:18px">
                  @if ($trans_historie->paid_by == 1)
                    <span class="badge bg-info">{{ __('trans_histories.cash') }}</span>
                  @elseif ($trans_historie->paid_by == 2)
                    <span class="badge bg-success">{{ __('trans_histories.transfer') }}</span>
                  @endif</td>
                  <td>{{ $trans_historie->note }}</td>
                </tr>
                
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@push('js')
<script type="text/javascript">
// active user 
  function activeStatus(id)
   {
      var id = id;
      var url = '{{ route("lich-su-giao-dich.update", ":id") }}';
      url = url.replace(':id', id);
      $("#id").val(id);
      $("#changeForm").attr('action', url);
    }
     function formActiveSubmit()
     {
        $("#changeForm").submit();
     }
</script>
@endpush


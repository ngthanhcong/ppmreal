@extends('layouts.master')
@section('title')
 {{ __('users.staff')  }}
@stop

@php
  function arr_permission($user_id)
  {
    $permissions = App\User::join('users_permissions', 'users.id', '=', 'users_permissions.user_id')
            ->join('permissions', 'users_permissions.permission_id', '=', 'permissions.id')
            ->where('users.id', $user_id)
            ->select('permissions.name')
            ->get();
    return $permissions;
  }
  function arr_role($user_id)
  {
    $roles = App\User::join('users_roles', 'users.id', '=', 'users_roles.user_id')
            ->join('roles', 'users_roles.role_id', '=', 'roles.id')
            ->where('users.id', $user_id)
            ->select('roles.name')
            ->get();
    return $roles;
  }

  function user_name($user_id)
    {
        $user_name = \App\User::where('id', $user_id)->first();
        return $user_name->fullname;
    }

@endphp

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.staff')}}</h1>
          </div> 
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('user-sale.list_staff') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div style="width:auto">
                <button type="button" data-toggle="modal" data-target="#modalCreateForm" type="button" class="btn btn-outline-secondary ">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('user-sale.user_sale_create') }}
                </button>
                <!-- <button type="button" class="btn btn-outline-secondary ">
                  <i class="fa fa-refresh" aria-hidden="true" data-toggle="tooltip" title="Làm mới"></i>
                </button> -->
              </div>
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>{{ __('users.fullname') }}</th>
                  <th>{{ __('users.username') }}</th>
                  <th>{{ __('users.email') }}</th>
                  <th>{{ __('users.active') }}</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                <tr>
                  <td>{{ $user->fullname }}</td>
                  <td>{{ $user->username }}</td>
                  <td>{{ $user->email }}</td>
                  <td style="font-size:18px">
                  @if($user->active =='1')
                    <span class="badge bg-success">{{ __('users.active')}}</span>
                  @else
                    <span class="badge bg-danger">{{ __('users.not_active')}}</span>
                  @endif
                  </td>
                  <td>
                  <a href="{{ route('user-sale.show', $user->id) }}" > 
                      <i data-toggle="tooltip" title="Xem chi tiết" style="text-align: right;" class="fa fa-eye" aria-hidden="true"></i>
                  </a>| 
                    <a href="{{ route('user-sale.edit',$user->id) }}"> 
                      <i data-toggle="tooltip" title="Chỉnh sửa" style="text-align: right;" class="	fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>  
                    | 
                    <a href="javascript(0);" onclick="deleteData({{$user->id}})"  style="color:red" data-toggle="modal" data-target="#confirm" > 
                      <i data-toggle="tooltip" title="Xóa" style="text-align: right;" class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                    <div class="modal fade" id="confirm"  role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body " style="background-image: url({{ asset('dist/img/delete.jpg') }}); background-repeat: no-repeat; background-size: 130px; padding-left: 130px;">
                          <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('users.user_delete') }}</h3>
                          <form method="post" id="deleteForm" action="">
                              @csrf
                              @method('DELETE')
                              <input type="hidden" name="id" id="id">
                              <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                              <i class="fa fa-times" aria-hidden="true"></i>
                                  {{ __('users.cancel') }}
                              </button>
                              <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                                {{ __('users.delete') }}
                              </button>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@include('user-sale.create')
<!-- /.modal -->

@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@push('js')
<script type="text/javascript">
  function deleteData(id)
   {
      var id = id;
      var url = '{{ route("user-sale.destroy", ":id") }}';
      url = url.replace(':id', id);
      $("#id").val(id);
      $("#deleteForm").attr('action', url);
    }

     function formSubmit()
     {
        $("#deleteForm").submit();
     }
     
</script>
@endpush

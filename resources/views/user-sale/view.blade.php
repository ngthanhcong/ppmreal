@extends('layouts.master')
@section('title')
 {{ __('users.user_view')  }}
@stop

@php
  function arr_permission($user_id)
  {
    $permissions = App\User::join('users_permissions', 'users.id', '=', 'users_permissions.user_id')
            ->join('permissions', 'users_permissions.permission_id', '=', 'permissions.id')
            ->where('users.id', $user_id)
            ->select('permissions.name')
            ->get();

    return $permissions;
  }
  function arr_role($user_id)
  {
    $roles = App\User::join('users_roles', 'users.id', '=', 'users_roles.user_id')
            ->join('roles', 'users_roles.role_id', '=', 'roles.id')
            ->where('users.id', $user_id)
            ->select('roles.name')
            ->get();

    return $roles;
  }

@endphp

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.user_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.user') }}</li>
            </ol>
          </div>
        </div>
      </div> -->
      <!-- /.container-fluid -->
    <!-- </section> -->
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
             <h3 class="card-title"> {{ __('users.user_view')  }}: {{$user->fullname}}</h3>
            </div>  
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group" style="width:auto; margin-top: -15px; margin-bottom: -10px;" >
                <button type="button" class="btn btn-outline-secondary">
                    <a style="text-decoration: none; color: black;" href="{{ redirect()->getUrlGenerator()->previous() }}">
                      <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('users.back') }}
                    </a>
                </button>
              </div>
              <hr />
                  <div class="row">
                    <div class="col-sm-4">
                    <table style="">
                          <tr class="form-group">
                            <td style="padding:5px"><b> {{ __('users.username')  }}</b></td>
                            <td>{{$user->username}}</td>
                          </tr>
                          <tr class="form-group">
                            <td style="padding:5px"><b> {{ __('users.fullname')  }}</b></td>
                            <td>{{$user->fullname}}</td>
                          </tr>
                          <tr class="form-group">
                            <td style="padding:5px"><b> {{ __('users.address')  }}</b></td>
                            <td>
                              @if($user->address == NULL)
                                {{ __('users.not_info')  }}
                              @else
                                {{$user->address}}
                              @endif
                            </td>
                          </tr>
                          <tr class="form-group">
                            <td style="padding:5px"><b> {{ __('users.email')  }}</b></td>
                            <td>{{$user->email}}</td>
                          </tr>
                          <tr class="form-group">
                            <td style="padding:5px"><b> {{ __('users.birthday')  }}</b></td>
                            <td>
                                @if($user->birthday==NULL)
                                  {{ __('users.not_info')  }}
                                @else
                                  {{ date('d/m/Y', strtotime($user->birthday)) }}
                                @endif
                            </td>
                          </tr>
                          <tr class="form-group">
                            <td style="padding:5px"><b> {{ __('users.gender')  }}</b></td>
                            <td>
                            @if($user->gender =='1')
                                Nam
                              @elseif($user->gender =='2')
                                Nữ
                              @else
                                {{ __('users.not_info')  }}
                              @endif
                              </td>
                          </tr>
                          <tr class="form-group">
                            <td style="padding:5px"><b> {{ __('users.phone')  }}</b></td>
                            <td>
                              @if($user->phone ==NULL)
                                {{ __('users.not_info')  }}
                              @else
                                {{$user->phone}}
                              @endif
                              </td>
                          </tr>
                          <tr class="form-group">
                            <td>
                            <a href="{{ route('user-sale.edit',$user->id) }}"> 
                              <button type="button" class="btn btn-outline-secondary">
                                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ __('user-sale.update_profile') }}
                              </button>
                            </a>
                            </td>
                            
                          </tr>
                        </table>
                    </div>
                    <div class="col-sm-4">
                    <div class="form-group" >
                      <b>{{ __('users.permission') }}: </b>
                        <ul>
                            @foreach (arr_permission($user->id) as $key)
                        <li>{{  $key->name }}.</li>
                            @endforeach
                            </ul>
                        </div>

                        <div class="form-group" >
                            <b>{{ __('users.role') }}: </b>
                                @foreach (arr_role($user->id) as $key)
                                    {{  $key->name }} <br/>
                                @endforeach
                            </div>
                          </div>
                    </div>
                  </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@endpush

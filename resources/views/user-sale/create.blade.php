<div class="modal fade" id="modalCreateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header card-primary">
            <!-- <h4 class="modal-title w-100 font-weight-bold">Sign up</h4> -->
            <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('user-sale.infomation') }}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body mx-3">
      <form method="post" action="{{ route('user-sale.store') }}">
        @csrf
        @method('POST')
        <div class="md-form mb-3">
            <label for="username">{{ __('user-sale.username') }} <sup class="text-danger">(*)</sup></label>
            <input required id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" autofocus>
            @if ($errors->has('username'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('username') }}</strong>
              </span>
            @endif
        </div>
        <div class="md-form mb-3">
            <label for="fullname">{{ __('user-sale.fullname') }} <sup class="text-danger">(*)</sup></label>
            <input required id="fullname" type="text" class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" name="fullname" value="{{ old('fullname') }}" autofocus>
            @if ($errors->has('fullname'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('fullname') }}</strong>
              </span>
            @endif
        </div>
        <div class="md-form mb-3">
            <label for="pass_first">{{ __('user-sale.password') }} <sup class="text-danger">(*)</sup></label>
            <input required id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" autofocus>
            @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
       </div>
      <div class="md-form mb-3">
            <label for="email">{{ __('user-sale.email') }} <sup class="text-danger">(*)</sup></label>
            <input required id="email" type="email"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus>
            @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
        </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-save" aria-hidden="true"></i>
            {{ __('user-sale.create') }}
        </button>
        <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
            <i class="fa fa-times" aria-hidden="true"></i>
            {{ __('user-sale.cancel') }}
        </button>
      </div>
      </form>
    </div>
  </div>
</div>

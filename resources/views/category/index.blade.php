@extends('layouts.master')
@section('title')
 {{ __('users.category')  }}
@stop

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.real_estate_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.category') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.category') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div style="width:auto">
                <button type="button" data-toggle="modal" data-target="#modalCreateForm" class="btn btn-outline-secondary">
                  <i class="fa fa-plus" aria-hidden="true"></i> {{ __('category.add') }}
                </button>
                <!-- <button id="refresh" type="button" class="btn btn-outline-secondary">
                  <i  class="fa fa-refresh" aria-hidden="true" data-toggle="tooltip" title="Làm mới"></i>
                </button> -->
              </div>
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>{{ __('category.category_name') }}</th>
                  <th>{{ __('category.note') }}</th>
                  <th>{{ __('category.action') }}</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($categories as $cat)
                <tr>
                  <td>{{ $cat->category_name }}</td>
                  <td >
                    {{ $cat->note }}
                  </td>
                  <td>
                    <a href="javascript(0);" data-toggle="modal" data-target="#modalEditForm{{$cat->id}}"> 
                      <i data-toggle="tooltip" title="Chỉnh sửa" style="text-align: right;" class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>  | 
                    <a href="javascript(0);" onclick="deleteData({{$cat->id}})"  data-toggle="modal" data-target="#confirm" > 
                      <i data-toggle="tooltip" style="color:red" title="Xóa" style="text-align: right;" class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                  </td>
                  <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                      aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-body " style="background-image: url({{ asset('dist/img/delete.jpg') }}); background-repeat: no-repeat; background-size: 130px; padding-left: 130px;">
                            <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('category.delete_category') }}</h3>
                            <form method="post" id="deleteForm" action="">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="id" id="id">
                                <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                    {{ __('category.cancel') }}
                                </button>
                                <button type="submit" name="" class="btn btn-danger" ata-dismiss="modal" onclick="formSubmit()">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                    {{ __('category.delete') }}
                                </button>
                            </form>
                            </div>
                        </div>
                      </div>
                    </div>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
<!-- Modal -->
@include('category.create')
@if (empty($categories))
  @include('category.edit')
@elseif (isset($categories))
  @foreach ($categories as $cat)
    @include('category.edit')
  @endforeach
@endif
<!-- /.modal -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@push('js')
<script type="text/javascript">
  function deleteData(id)
   {
      var id = id;
      var url = '{{ route("category.destroy", ":id") }}';
      url = url.replace(':id', id);
      $("#id").val(id);
      $("#deleteForm").attr('action', url);
    }

     function formSubmit()
     {
        $("#deleteForm").submit();
     }
     
</script>
@endpush



<div class="modal fade" id="modalCreateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header card-primary">
            <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('category.create_category') }}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body mx-3 needs-validation" novalidate>
        <form method="post" action="{{ route('category.store') }}">
            @csrf
            @method('POST')
            @include('category.form.create')
        </form>
    </div>
  </div>
</div>

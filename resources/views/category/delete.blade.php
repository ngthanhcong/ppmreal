<div class="modal fade" id="confirm{{ $cat->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body " style="background-image: url({{ asset('dist/img/delete.jpg') }}); background-repeat: no-repeat; background-size: 130px; padding-left: 130px;">
        <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('category.delete_category') }}</h3>
        <form method="post" action="{{ route('category.update', $cat->id) }}">
            @csrf
            @method('DELETE')
            <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
            <i class="fa fa-times" aria-hidden="true"></i>
                {{ __('category.cancel') }}
            </button>
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-trash" aria-hidden="true"></i>
                {{ __('category.delete') }}
            </button>
        </form>
        </div>
    </div>
  </div>
</div>
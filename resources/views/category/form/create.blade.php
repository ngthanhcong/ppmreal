<div class="md-form mb-3">
    <label for="category_name">{{ __('category.category_name') }}</label>
    <input id="category_name" type="text" class="form-control {{ $errors->has('category_name') ? ' is-invalid' : '' }}" name="category_name" value="{{ old('category_name') }}" autofocus required>
    @if ($errors->has('category_name'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('category_name') }}</strong>
        </span>
    @endif
</div>
<div class="md-form mb-3">
    <label for="note">{{ __('category.note') }}</label>
    <input id="note" type="text" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" value="{{ old('note') }}" autofocus>
</div>
    </div>
<div class="modal-footer d-flex justify-content-center">
    <button type="submit" class="btn btn-success">
        <i class="fa fa-save" aria-hidden="true"></i>
            {{ __('category.save') }}
    </button>
    <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
        <i class="fa fa-times" aria-hidden="true"></i>
            {{ __('category.cancel') }}
    </button>
</div>
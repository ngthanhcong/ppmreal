@extends('layouts.master')
@section('title')
 {{ __('users.permission')  }}
@stop

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.permission')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.permission') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.permission') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div style="width:auto">
                <button type="button" data-toggle="modal" data-target="#modalCreateForm" type="button" class="btn btn-outline-secondary ">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('permission.per_create') }}
                </button>
                <button type="button" class="btn btn-outline-secondary ">
                  <i class="fa fa-refresh" aria-hidden="true" data-toggle="tooltip" title="Làm mới"></i>
                </button>
              </div>
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>{{ __('permission.permission_name') }}</th>
                  <th>{{ __('permission.note') }}</th>
                  <th>{{ __('permission.created_at') }}</th>
                  <th>{{ __('permission.updated_at') }}</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($per as $per)
                <tr>
                  <td>{{$per->slug }}</td>
                  <td>{{$per->name}}</td>
                  <td>{{ date('d/m/Y H:i:s', strtotime($per->created_at)) }} </td>
                  <td>{{ date('d/m/Y H:i:s', strtotime($per->updated_at)) }}</td>
                  <td>
                    <a href="{{ route('permission.edit',$per->id) }}"> 
                      <i data-toggle="tooltip" title="Chỉnh sửa" style="text-align: right;" class="	fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>  
                    | 
                    <a href="javascript(0);" onclick="deleteData({{$per->id}})" data-toggle="modal" data-target="#confirm" > 
                      <i data-toggle="tooltip" style="color:red " title="Xóa" style="text-align: right;" class="fa fa-trash-o" aria-hidden="true" ></i>
                    </a>
                  </td>
                </tr>
                <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body " style="background-image: url({{ asset('dist/img/delete.jpg') }}); background-repeat: no-repeat; background-size: 130px; padding-left: 130px;">
                          <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('permission.per_del') }}</h3>
                          <form method="post" id="deleteForm" action="">
                              @csrf
                              @method('DELETE')
                              <input type="hidden" name="id" id="id">
                              <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                              <i class="fa fa-times" aria-hidden="true"></i>
                                  {{ __('users.cancel') }}
                              </button>
                              <button data-dismiss="modal" onclick="formSubmit()"  name="" type="submit" class="btn btn-danger">
                                  <i class="fa fa-trash" aria-hidden="true"></i>
                                      {{ __('permission.delete') }}
                              </button>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@include('permission.create')
<!-- /.modal -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@push('js')
<script type="text/javascript">
  function deleteData(id)
   {
      var id = id;
      var url = '{{ route("permission.destroy", ":id") }}';
      url = url.replace(':id', id);
      $("#id").val(id);
      $("#deleteForm").attr('action', url);
    }

     function formSubmit()
     {
        $("#deleteForm").submit();
     }
     
</script>
@endpush
@extends('layouts.master')
@section('title')
 {{ __('users.permission')  }}
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.permission')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.permission') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
             <h3 class="card-title">Xem quyền: {{$per->name}}</h3>
            </div>  
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group" style="width:auto; margin-top: -15px; margin-bottom: -10px;" >
                <button type="button" class="btn btn-outline-secondary">
                    <a style="text-decoration: none; color: black;" href="{{ redirect()->getUrlGenerator()->previous() }}">
                      <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('users.back') }}
                    </a>
                </button>
              </div>
                  <hr />   
              <table style="margin-left:20px;width:700px">
                <tr>
                  <td style="width:150px;padding:5px"><b>Tên quyền</b></td>
                  <td>{{$per->slug}}</td>
                </tr>
                <tr>
                  <td style="width:150px;padding:5px"><b>Chú thích</b></td>
                  <td>{{$per->name}}</td>
                </tr>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@endpush

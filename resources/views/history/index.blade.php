@extends('layouts.master')
@section('title')
 {{ __('users.histories_advisory')  }}
@stop
@php
    function user_name($user_id)
    {
        $user_name = \App\User::where('id', $user_id)->first();
        return $user_name->fullname;
    }
    function customer_name($customer_id)
    {
        $customer_name = \App\Customer::where('id', $customer_id)->first();
        return $customer_name->customer_name;
    }
    function product_name($product_id)
    {
        $product_name = \App\Product::where('id', $product_id)->first();
        return $product_name->product_name;
    }
@endphp
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.histories_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.histories_advisory') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.histories_advisory') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <!-- <div style="width:auto">
                <button type="button" data-toggle="modal" data-target="#modalCreateForm" type="button" class="btn btn-default ">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('role.role_create') }}
                </button>
                <button type="button" class="btn btn-default ">
                  <i class="fa fa-refresh" aria-hidden="true" data-toggle="tooltip" title="Làm mới"></i>
                </button>
              </div> -->
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th>{{ __('history.user_name') }}</th>
                  <th>{{ __('history.customer_name') }}</th>
                  <th>{{ __('history.product_name') }}</th>
                  <th>{{ __('history.consulting_status') }}</th>
                  <th>{{ __('history.consulting_at') }}</th>
                  <th>{{ __('history.note') }}</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($histories as $history)
                <tr>
                  <td>{{ user_name($history->user_id) }}</td>
                  <td>{{ customer_name($history->customer_id) }}</td>
                  <td>{{ product_name($history->product_id) }}</td>
                  <td style="font-size:18px">
                  @if ($history->consulting_status == 0)
                    <span class="badge bg-info">Đang tư vấn</span>
                  @elseif ($history->consulting_status == 1)
                    <span class="badge bg-success">Tư vấn thành công</span>
                  @elseif ($history->consulting_status == 2)
                    <span class="badge bg-danger">Tư vấn thất bại</span>
                  @endif
                  </td>
                  <td>{{ date('d/m/Y H:i:s', strtotime($history->consulting_at)) }}</td>
                  <td>{{ $history->note }}</td>
                  <td>
                    <a href="{{ route('history.show', $history->id) }}" > 
                        <i data-toggle="tooltip" title="Xem chi tiết" style="text-align: right;" class="fa fa-eye" aria-hidden="true"></i>
                    </a>| 
                    <a href="{{ route('history.edit',$history->id) }}"> 
                        <i data-toggle="tooltip" title="Chỉnh sửa" style="text-align: right;" class="	fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
<!-- /.modal -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush
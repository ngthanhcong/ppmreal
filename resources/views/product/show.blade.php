@extends('layouts.master')

@section('title')
 {{ $product->product_name  }}
@stop 
@php
function get_address($ward_id)
   {
		$ward = \App\Ward::where('id', $ward_id)->first();
		$district = \App\District::where('id', $ward->district_id)->first();
		$town = \App\Town::where('id', $district->town_id)->first();
		$address = $ward->ward_name .', '. $district->district_name .', '. $town->town_name ;
		return $address;
   }
function get_category($category_id)
	{
		$category = \App\Category::where('id', $category_id)->first();
		return $category->category_name;
	}
@endphp


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.real_estate_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.product') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $product->product_name }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
					<div class="form-group" style="width:auto; margin-top: -15px; margin-bottom: -10px;" >
						<button type="button" class="btn btn-outline-secondary">
							<a style="text-decoration: none; color: black;" href="{{ route('product.index') }}">
								<i class="fa fa-arrow-left" aria-hidden="true"></i> 
								{{ __('Trở lại') }}
							</a>
						</button>
										
					</div>
					<!-- <div style="right: 0; margin-top: -35px; margin-bottom: -10px; text-align: right;"> 
						<button type="button" class="btn btn-outline-secondary">
							<a style="text-decoration: none; color: black; text-align:right" href="{{ route('sale.index') }}">
								<i class="fa fa-arrow-left" aria-hidden="true"></i> 
								{{ __('Trở lại bản đồ') }}
							</a>
						</button>
            </div> -->
					<hr />
					<div class="container" style="margin-left: -10px;" >
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<b>{{ __('product.category_id') }}: </b>{{ get_category($product->category_id) }}
							</div>
							<div class="form-group">
								<b>{{ __('product.ward_id') }}: </b> 
								{{ get_address($product->ward_id) }}
							</div>
							<div class="form-group">
								<b>{{ __('product.price') }}: </b>
								@if (isset($product->price))
									{{ number_format($product->price, 0) }} VND 
								@else
									<span class="badge bg-success">
											{{ __('Thỏa thuận') }}
									</span>
								@endif
							</div>
							<div class="form-group">
								<b>{{ __('product.acreage') }}: </b>{{ $product->acreage }} m<sup>2</sup>
							</div>

							<div class="form-group" >
								<b>{{ __('product.direction') }}: </b>
								@foreach (__('product.directions') as $key => $drt )
									@if ($key == $product->direction)
										{{ $drt['vi'] . ' (' . $drt['en'] . ')' }}
										@endif
								@endforeach
							</div>

							<div class="form-group">
								<b>{{ __('product.content') }}: </b>
								<div>
									{!! $product->content !!}
								</div>
							</div>
						</div>

					<div class="col-6">
						<div class="form-group" >
							<label for="area">{{ __('Vị trí bản đồ') }}</label>
							@include('product.form.showmap')
						</div>
					</div>
				</div>
				</div>
				</div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
@extends('layouts.master')

@section('title')
 {{ $product->product_name }}
@stop 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.real_estate_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.product') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $product->product_name }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <form method="post" action="{{ route('product.update', $product->id) }} " enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group" style="width:auto; margin-top: -15px; margin-bottom: -10px;" >
                    <button type="button" class="btn btn-outline-secondary">
                        <a style="text-decoration: none; color: black;" href="{{ redirect()->getUrlGenerator()->previous() }}">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('product.back') }}</a>
                    </button>
                    <div style="right: 0; margin-top: -35px; margin-bottom: -10px; text-align: right;"> 
                    <button type="reset" class="btn btn-outline-secondary">
                          <i class="fa fa-times" aria-hidden="true"></i>
                              {{ __('product.cancel') }}
                      </button>
                      <button type="submit" class="btn btn-success">
                          <i class="fa fa-save" aria-hidden="true"></i>
                              {{ __('product.save') }}
                      </button>
                    </div>
                </div>
			          <hr />
                <div class="container" style="margin-left: -10px;" >
                    @include('product.form.edit')
                </div>
			      </div>
            </form>
            <!-- card-body -->
			</div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@endsection
<style>
 #map-canvas-show {
        height: 450px;
        margin: 0;
        padding: 0px
      }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-uDYSJbafK6Ptn_JaehA5avxHSPxkcNI&libraries=drawing&callback=initialize"
></script>
<script>
function initialize() {
    var default_vitri = {lat: 9.9650508, lng: 105.5096585 };
    var zoom_default = 9;
    var vitri;
    var name = '{!! $product->product_name !!}';
    // Create a map object and specify the DOM element
    // for display.
    @if (empty($product->lat))
        default_vitri = default_vitri;
        zoom_default = 9;
    @else
        default_vitri = {lat: {{ ( $product->lat) }} , lng: {{( $product->lng ) }}  };
        zoom_default = 14 ;
    @endif
    var map = new google.maps.Map(document.getElementById('map-canvas-show'), {
        center: default_vitri,
        zoom: zoom_default
    });
    // Create a marker and set its position.
    @if (!empty($product->lat))
        vitri = {lat: {{ ( $product->lat) }} , lng: {{( $product->lng ) }}  };    
        var marker = new google.maps.Marker({
        map: map,
        position: vitri,
        title: name
    });
    @endif
    
    // Define the LatLng coordinates for the polygon's path.
    var triangleCoords = [
        {{ $product->area }},
        ];
    @if ($product->status == 0)
        var strokeColor = "#FF0000";
        var fillColor = "#FF0000";
    @else
        var strokeColor = "#0cc96d";
        var fillColor = "#0cc96d";
    @endif

    // Construct the polygon.
    var bermudaTriangle = new google.maps.Polygon({

        paths: triangleCoords,
        strokeColor: strokeColor,
        fillColor: fillColor,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillOpacity: 0.35
    });
    bermudaTriangle.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-canvas-show"></div>
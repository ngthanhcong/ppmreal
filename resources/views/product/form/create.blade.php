<!-- <div class="container"> -->
  <div class="row">
    <div class="col-5">

		<div class="md-form mb-3">
            <label for="avatar">{{ __('product.avatar') }}: &emsp;</label>
            <img id="avatar_show" src="{{ asset('uploads/images/upload_img.jpg')}}" style="max-width:180px;" /><br>
        	<input onchange="readURL(this);" name="avatar" type="file" id="avatar_upload"/>  
        </div>
		<div class="md-form mb-3">
			<label for="product_name">{{ __('product.product_name') }} <sup class="text-danger">(*)</sup></label>
			<input onchange="str_to_alias('product_name', 'alias');" id="product_name" type="text" class="form-control {{ $errors->has('product_name') ? ' is-invalid' : '' }}" name="product_name" value="{{ old('product_name') }}" autofocus >
			@if ($errors->has('product_name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('product_name') }}</strong>
				</span>
			@endif
		</div>
		
		<div class="md-form mb-3">
			<label for="category_id">{{ __('product.category_id') }} <sup class="text-danger">(*)</sup></label>
            <select id="category_id" type="text" class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}" name="category_id" value="{{ old('category_id') }}"  autofocus>
				<option value="" selected>-- Chọn --</option>
            @foreach (\App\Category::get() as $cat)
                <option value="{{ $cat->id }}">{{ $cat->category_name }}</option>
            @endforeach
            </select>
            
			@if ($errors->has('category_id'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('category_id') }}</strong>
				</span>
			@endif
		</div>

		<div class="md-form mb-3">
			<label for="price">{{ __('product.price') }}</label>
			<div class="input-group mb-3"> 
				<input  aria-describedby="priceHelpBlock" id="price" type="number" min="1000000" max="100000000000000" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" name="price"  autofocus>
				<div class="input-group-append">
						<span class="input-group-text">VND</span>
				</div>
				@if ($errors->has('price'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('price') }}</strong>
					</span>
				@endif
			</div>	
			<small id="priceHelpBlock">
					Giá là thỏa thuận hoặc một con số cụ thể. Ví dụ: 1000000.
			</small>
		</div>

		<div class="md-form mb-3">
			<label for="ward_id">{{ __('product.ward_id') }} <sup class="text-danger">(*)</sup></label>
			<select class="form-control select2 {{ $errors->has('ward_id') ? ' is-invalid' : '' }}" name="ward_id" id="ward_id" value="{{ old('ward_id') }}" style="width: 100%;">
				<option value="" selected>-- Chọn --</option>
				@foreach (\App\Ward::get() as $ward)
					@if(empty(old('ward_id')))
						<option value="{{ $ward->id }}">{{ $ward->ward_name . ' - ' . $ward->district->district_name . ' - ' . $ward->district->town->town_name }}</option>
					@else
						<option value="{{ old('ward_id') }}">{{ $ward->ward_name . ' - ' . $ward->district->district_name . ' - ' . $ward->district->town->town_name }}</option>
					@endif
                @endforeach
            </select>
			@if ($errors->has('ward_id'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('ward_id') }}</strong>
				</span>
			@endif
		</div>

		<div class="md-form mb-3">
			<label for="acreage">{{ __('product.acreage') }} <sup class="text-danger">(*)</sup></label>
            <div class="input-group mb-3">  
                <input id="acreage" type="number" min="1" class="form-control {{ $errors->has('acreage') ? ' is-invalid' : '' }}" name="acreage" value="{{ old('acreage') }}" autofocus>
                <div class="input-group-append">
                    <span class="input-group-text">m<sup>2</sup></span>
				</div>
				@if ($errors->has('acreage'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('acreage') }}</strong>
				</span>
				@endif
            </div>
		</div>
        
        <div class="md-form mb-3">
			<label for="length">{{ __('product.length') }} </label>
            <div class="input-group mb-3">  
            <input id="length" type="number" min="1" class="form-control {{ $errors->has('length') ? ' is-invalid' : '' }}" name="length" value="{{ old('length') }}" autofocus >
                <div class="input-group-append">
                    <span class="input-group-text">m</span>
				</div>
				@if ($errors->has('length'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('length') }}</strong>
				</span>
				@endif
            </div>
			
		</div>

        <div class="md-form mb-3">
			<label for="width">{{ __('product.width') }} </label>
            <div class="input-group mb-3">  
			<input id="width" type="number" min="1" class="form-control {{ $errors->has('width') ? ' is-invalid' : '' }}" name="width" value="{{ old('width') }}" autofocus >            
                <div class="input-group-append">
                    <span class="input-group-text">m</span>
				</div>
				@if ($errors->has('width'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('width') }}</strong>
				</span>
				@endif
            </div>
			
		</div>

		<div class="md-form mb-3">
			<label for="direction">{{ __('product.direction') }} <sup class="text-danger">(*)</sup></label>
            <select id="direction" class="form-control {{ $errors->has('direction') ? ' is-invalid' : '' }}" name="direction" value="{{ old('direction') }}" autofocus >
				<option value="" selected>-- Chọn --</option>
                @foreach (__('product.directions') as $key => $drt )
                    <option value="{{ $key }}">{{ $drt['vi'] . ' (' . $drt['en'] . ')' }}</option>
                @endforeach
            </select>
			@if ($errors->has('direction'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('direction') }}</strong>
				</span>
			@endif
		</div>

		<div class="md-form mb-3">
			<label for="status">{{ __('product.status') }} <sup class="text-danger">(*)</sup></label>
			<select id="status"  class="form-control {{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" value="{{ old('status') }}" autofocus>
				<option value="" selected>-- Chọn --</option>
              	<option value="1">Đang bán</option>
				<option value="2">Đã bán</option>
            </select>
			@if ($errors->has('status'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('status') }}</strong>
				</span>
			@endif
		</div>
		
		<div class="md-form mb-3">
            <label for="note">{{ __('product.note') }}</label>
            <input id="note" type="text" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" value="{{ old('note') }}" autofocus>
        </div>
			@if ($errors->has('status'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('status') }}</strong>
				</span>
			@endif
		
    </div>
    <div class="col-7">
		<div class="md-form mb-3" >
			<label for="summary">{{ __('product.summary') }}</label>
			<textarea id="summary" type="text" class="form-control {{ $errors->has('summary') ? ' is-invalid' : '' }}" name="summary" value="{{ old('summary') }}" autofocus ></textarea>
			@if ($errors->has('summary'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('summary') }}</strong>
				</span>
			@endif
		</div>

		<div class="md-form mb-3" >
			<label for="content">{{ __('product.content') }}</label>
			<textarea id="content" type="text" class="form-control ckeditor {{ $errors->has('content') ? ' is-invalid' : '' }}" name="content" value="{{ old('content') }}" autofocus ></textarea>
			@if ($errors->has('content'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('content') }}</strong>
				</span>
			@endif
		</div>
		<div class="md-form mb-3">
			<label for="area">{{ __('product.area') }}</label>
			<small>(Vui lòng vẽ chính xác)</small>
			@include('product.form.googlemap')
		</div>
		<input class="form-control" id="alias" name="alias" type="text" value="{{ old('alias') }}" hidden 	/>
    </div>
  </div>
<!-- </div> -->


<div class="row">
    <div class="col-5">
        <div class="md-form mb-3">
            <label for="avatar">{{ __('product.avatar') }}: &emsp;</label>
            @if (empty($product->avatar))
                <img id="avatar_show" src="{{ asset('uploads/images/upload_img.jpg')}}" style="max-width:180px;" /><br> 
                @else
                <img id="avatar_show" src="{{ url($product->avatar)}}" style="max-width:180px;" />
            @endif
            <input onchange="readURL(this);" name="avatar" type="file" id="avatar_upload"/>  
        </div>
        
        <div class="md-form mb-3">
            <label for="product_name">{{ __('product.product_name') }} <sup class="text-danger">(*)</sup></label>
            <input onchange="str_to_alias('product_name', 'alias');" id="product_name" type="text" class="form-control {{ $errors->has('product_name') ? ' is-invalid' : '' }}" name="product_name" value="{{ old('product_name', $product->product_name) }}" autofocus>
            @if ($errors->has('product_name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('product_name') }}</strong>
            </span>
            @endif
        </div>
                            
        <div class="md-form mb-3">
            <label for="category_id">{{ __('product.category_id') }} <sup class="text-danger">(*)</sup></label>
            <select id="category_id" type="text" class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}" name="category_id" value="{{ old('category_id',$product->product_name) }}" autofocus >
            @foreach (\App\Category::get() as $cat)
                <option value="{{ $cat->id }}" {{ $cat->id == $product->category_id ? 'selected' : '' }}>{{ $cat->category_name }}</option>
            @endforeach
            </select>
            @if ($errors->has('category_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif
        </div>

        <div class="md-form mb-3">
            <label for="price">{{ __('product.price') }}</label>
            <div class="input-group mb-3"> 
                <input id="price" type="number" min="1000000" max="100000000000000" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" value="{{ old('price',$product->price) }}" autofocus >
                <div class="input-group-append">
                    <span class="input-group-text">VND</span>
                </div>
            </div>
             @if ($errors->has('price'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
             @endif
        </div>

        <div class="md-form mb-3">
            <label for="ward_id">{{ __('product.ward_id') }} <sup class="text-danger">(*)</sup></label>
            <select type="text" class="form-control select2 {{ $errors->has('ward_id') ? ' is-invalid' : '' }}" name="ward_id" id="ward_id" style="width: 100%;" >
                @foreach (\App\Ward::get() as $ward)
                <option value="{{ $ward->id }}" {{ $ward->id == $product->ward_id ? 'selected' : '' }} >{{ $ward->ward_name . ' - ' . $ward->district->district_name . ' - ' . $ward->district->town->town_name }}</option>
                @endforeach
            </select>
            @if ($errors->has('ward_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('ward_id') }}</strong>
                </span>
            @endif
        </div>

        <div class="md-form mb-3">
            <label for="acreage">{{ __('product.acreage') }} <sup class="text-danger">(*)</sup></label>
            <div class="input-group mb-3">  
                <input id="acreage" type="text" min="1" class="form-control {{ $errors->has('acreage') ? ' is-invalid' : '' }}" name="acreage" value="{{ old('acreage',$product->acreage) }}" autofocus>
                <div class="input-group-append">
                    <span class="input-group-text">m<sup>2</sup></span>
                </div>
            </div>
            @if ($errors->has('acreage'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('acreage') }}</strong>
                </span>
            @endif
        </div>
        
        <div class="md-form mb-3">
            <label for="length">{{ __('product.length') }}</label>
            <div class="input-group mb-3">  
            <input id="length" type="text" min="1" class="form-control {{ $errors->has('length') ? ' is-invalid' : '' }}" name="length" value="{{ old('length',$product->length) }}" autofocus >
                <div class="input-group-append">
                    <span class="input-group-text">m</span>
                </div>
            </div>
            @if ($errors->has('length'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('length') }}</strong>
                </span>
            @endif
        </div>

        <div class="md-form mb-3">
            <label for="width">{{ __('product.width') }}</label>
            <div class="input-group mb-3">  
            <input id="width" type="text" min="1" class="form-control {{ $errors->has('width') ? ' is-invalid' : '' }}" name="width" value="{{ old('width',$product->width) }}" autofocus >            
                <div class="input-group-append">
                    <span class="input-group-text">m</span>
                </div>
            </div>
            @if ($errors->has('width'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('width') }}</strong>
                </span>
            @endif
        </div>

        <div class="md-form mb-3">
            <label for="direction">{{ __('product.direction') }}</label>
            <select id="direction" type="text" class="form-control {{ $errors->has('direction') ? ' is-invalid' : '' }}" name="direction" value="{{ old('direction') }}" autofocus >
                @foreach (__('product.directions') as $key => $drt )
                    <option value="{{ $key }}" {{ $key == $product->direction ? 'selected' : '' }}>{{ $drt['vi'] . ' (' . $drt['en'] . ')' }}</option>
                @endforeach
            </select>
            @if ($errors->has('direction'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('direction') }}</strong>
                </span>
            @endif
        </div>

        <div class="md-form mb-3">
            <label for="status">{{ __('product.status') }}</label>
            <select class="form-control" id="status" type="text" class="form-control {{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" value="{{ old('status') }}" autofocus>
                <option value="1" {{ 1 == $product->status ? 'selected' : '' }}>Đang bán</option>
                <option value="2" {{ 2 == $product->status ? 'selected' : '' }}>Đã bán</option>
                <option value="3" {{ 3 == $product->status ? 'selected' : '' }}>Đang tư vấn</option>
            </select>
            @if ($errors->has('status'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
            @endif
        </div>

        
        
        <div class="md-form mb-3">
            <label for="note">{{ __('product.note') }}</label>
            <input id="note" type="text" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" value="{{ old('note', $product->note) }}" autofocus>
        </div>
    </div>
    <div class="col-7">
        <div class="md-form">
            <label for="summary">{{ __('product.summary') }}</label>
            <textarea id="summary" type="text" class="form-control" name="summary">{{ $product->summary }}</textarea>
            @if ($errors->has('summary'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('summary') }}</strong>
                </span>
            @endif
        </div>

        <div class="md-form">
            <label for="content">{{ __('product.content') }}</label>
            <textarea id="content" class="form-control ckeditor" name="content">{{ $product->content }}</textarea>
            @if ($errors->has('content'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
            @endif
        </div>

        <div style="right: 0; margin-bottom: -5px; text-align: right;"> 
            <button type="button" class="show-btn fa fa-pencil" data-toggle="tooltip" title="Chỉnh sửa"></button>
            <button type="button" class="hide-btn fa fa-times" data-toggle="tooltip" title="Hủy bỏ"></button> 
        </div>
        <div class="md-form googlemap" id="googlemap">
            <label for="area" >{{ __('product.area') }}</label>
            @include('product.form.googlemap')
        </div>
        <div class="md-form showmap" id="showmap">
            <label for="area" >{{ __('product.area') }}</label>
            @include('product.form.showmap')
        </div>
        
        <input class="form-control" id="alias" name="alias" type="text" value="{{ old('alias', $product->alias) }}" hidden /> 
        
    </div>
</div>	
@push('js')
<!-- Load img before update -->
<script src="{{ asset('dist/js/plugins/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#avatar_show').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar_upload").change(function(){
        readURL(this);
    });
</script>
@endpush

@push('ready')
<script type="text/javascript">
$(document).ready(function(){
    $(".showmap").show();
    $(".googlemap").hide();
    // Hide googlemap (when update new googlemap )
    $(".hide-btn").click(function(){
        $(".googlemap").hide();
        $(".showmap").show();
    });
    // Show hidden showmap
    $(".show-btn").click(function(){
        $(".showmap").hide();
       	$(".googlemap").show();
    });
});

</script>
@endpush


<style>
 #map-canvas {
        height: 455px;
        margin: 0;
        padding: 0px
      }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-uDYSJbafK6Ptn_JaehA5avxHSPxkcNI&libraries=drawing"></script>
<script>
function initialize() {
    var lon_lat_array;
    var polygon; 
    var results_array = new Array();
    var lat_arr = new Array();
    var lng_arr = new Array();
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    var vietnam = { lat: 10.0101015, lng: 105.7769319 };
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
      zoom: 11,
      center: vietnam
    });
    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: ['polygon']
        },
        circleOptions: {
            fillColor: '#ffff00',
            fillOpacity: 1,
            strokeWeight: 5,
            clickable: false,
            editable: true,
            zIndex: 1
        }
    });
    drawingManager.setMap(map);
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
		// console.log(event.overlay);
    // polygon = event.overlay;    
		lon_lat_array = event.overlay.getPath().getArray();
		// console.log(lon_lat_array);
		for(i=0;i<lon_lat_array.length;i++){
			complete_results = "{lat: " + lon_lat_array[i].lat() +  ", lng: " + lon_lat_array[i].lng() + " }";
      sum_lat = lon_lat_array[i].lat();
      lat_arr.push(sum_lat);
      sum_lng = lon_lat_array[i].lng();
      lng_arr.push(sum_lng);
			results_array.push(complete_results);
    }
    function sum(arr) {
      return arr.reduce(function (a, b) {
          return a + b;
      }, 0);
    }
    $('#lat').val(sum(lat_arr)/lat_arr.length);
    $('#lng').val(sum(lng_arr)/lng_arr.length);
    $('#area').val(results_array);
      google.maps.event.addDomListener(document, 'keyup', function (e) {
          
          var code = (e.keyCode ? e.keyCode : e.which);
          
          if (code === 27) {

              drawingManager.setDrawingMode(null);
              polygon.setMap(null);
          }
      });
    });

    

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<div id="map-canvas"></div>
<input class="form-control" id="area" name="area" type="text" hidden/>
<input class="form-control" id="lat" name="lat" type="text" hidden/>
<input class="form-control" id="lng" name="lng" type="text" hidden/>

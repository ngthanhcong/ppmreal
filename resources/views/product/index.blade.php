@extends('layouts.master')
@section('title')
 {{ __('users.product')  }}
@stop
@php
  function number_formatting($n) {
    // first strip any formatting;
    $n = (0+str_replace(",","",$n));
        
    // is this a number?
    if(!is_numeric($n)) return false;
        
    // now filter it;
    if($n>1000000000) return round(($n/1000000000),1).' tỷ';
    else if($n>1000000) return round(($n/1000000),1).' triệu';
    else if($n>1000) return round(($n/1000),1).' ngàn';
        
    return number_format($n);
    }
@endphp
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.real_estate_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.product') }}</li>
            </ol>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.product') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div style="width:auto">
                <button type="button" class="btn btn-outline-secondary">
                  <a style="text-decoration: none; color: black;" href="{{ route('product.create') }}" > 
                  <i class="fa fa-plus" aria-hidden="true"></i> {{ __('product.add') }}
                  </a>
                </button>
                <!-- <button id="refresh" type="button" class="btn btn-outline-secondary">
                  <i  class="fa fa-refresh" aria-hidden="true" data-toggle="tooltip" title="Làm mới"></i>
                </button> -->
              </div>
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <th style="width:250px">{{ __('product.product_name') }}</th>
                  <th style="width:65px">{{ __('product.avatar') }}</th>
                  <th style="width:120px">{{ __('product.category_id') }}</th>
                  <th style="width:65px">{{ __('product.price') }}</th>
                  <th style="width:70px">{{ __('product.status') }}</th>
                  <th style="width:70px">{{ __('product.acreage') }}</th>
                  <th style="width:115px">{{ __('product.user_id') }}</th>
                  <th style="width:55px">{{ __('product.action') }}</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($products as $product)
                    @php
                      $name = \App\User::where('id', $product->user_id)->first();
                      $category = \App\Category::where('id',$product->category_id)->first();
                    @endphp
                  <tr>
                    <td>{{ $product->product_name }}</td>
                    <td>
                      @if (empty($product->avatar))
                        <img src="{{ asset('uploads/images/no_image_available.jpeg') }}" style="width:120px; height:120px"  />
                      @else
                        <img src="{{ url($product->avatar)}}" style="width:120px; height:120px" />
                      @endif
                    </td>
                    
                    <td>{{ $category->category_name }}</td>
                    <td>
                    @if (isset($product->price))
                        {{ number_format($product->price, 0) }} VND 
                    @else
                      <span class="badge bg-success">
                        {{ __('Thỏa thuận') }}
                      </span>
                    @endif
                    </td>
                    <td style="font-size:18px;">
                    @if ($product->status == 1)
                      <span class="badge bg-info">
                        {{ __('Đang bán') }}
                      </span>
                    @elseif ($product->status == 2)
                      <span class="badge bg-success">
                        {{ __('Đã bán') }}
                      </span>
                    @elseif ($product->status == 3)
                      <span class="badge bg-warning">
                        {{ __('Đang tư vấn') }}
                      </span>
                    @endif
                    </td>
                    <td>{{ $product->acreage }} m<sup>2</sup> </td>
                    <td>{{ $name->fullname }}</td>
                  <td > 
                    <a  href="{{ route('product.show', $product->alias) }}" > 
                      <i data-toggle="tooltip" title="Xem chi tiết" style="text-align: right;" class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                    | 
                    <a  href="{{ route('product.edit', $product->id) }}" > 
                      <i data-toggle="tooltip" title="Chỉnh sửa" style="text-align: right;" class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a> 
                    |
                    <a href="javascript(0);" style="color:red" data-toggle="modal" data-target="#confirm{{$product->id}}" > 
                      <i data-toggle="tooltip" title="Xóa" style="text-align: right;" class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
<!-- /.content-wrapper -->
<!-- Modal -->
@foreach ($products as $product)
<div class="modal fade" id="confirm{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body " style="background-image: url({{ asset('dist/img/delete.jpg') }}); background-repeat: no-repeat; background-size: 130px; padding-left: 130px;">
        <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('product.delete_product') }}</h3>
        <h6>Bất động sản: {{ $product->product_name }}</h6>
        <form method="post" action="{{ route('product.destroy', $product->id) }}">
            @csrf
            @method('DELETE')
            <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
            <i class="fa fa-times" aria-hidden="true"></i>
                {{ __('product.cancel') }}
            </button>
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-trash" aria-hidden="true"></i>
                    {{ __('product.delete') }}
            </button>
        </form>
        </div>
    </div>
  </div>
</div>
@endforeach
<!-- /.modal -->
@endsection


@extends('layouts.master')
@section('title')
 {{ __('users.user')  }}
@stop

@php
  function arr_permission($user_id)
  {
    $permissions = App\User::join('users_permissions', 'users.id', '=', 'users_permissions.user_id')
            ->join('permissions', 'users_permissions.permission_id', '=', 'permissions.id')
            ->where('users.id', $user_id)
            ->select('permissions.name')
            ->get();
    return $permissions;
  }
  function arr_role($user_id)
  {
    $roles = App\User::join('users_roles', 'users.id', '=', 'users_roles.user_id')
            ->join('roles', 'users_roles.role_id', '=', 'roles.id')
            ->where('users.id', $user_id)
            ->select('roles.name')
            ->get();
    return $roles;
  }

  function user_name($user_id)
    {
        $user_name = \App\User::where('id', $user_id)->first();
        return $user_name->fullname;
    }

@endphp

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.user_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.user') }}</li>
            </ol>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('users.user') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div style="width:auto">
                <button type="button" data-toggle="modal" data-target="#modalCreateForm" type="button" class="btn btn-outline-secondary ">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('users.user_create') }}
                </button>
              </div>
              <br />
              <table id="tableData" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                  <!-- <th>{{ __('users.avatar') }}</th> -->
                  <th>{{ __('users.fullname') }}</th>
                  <th>{{ __('users.username') }}</th>
                  <!-- <th style="width:120px">{{ __('users.permission') }}</th> -->
                  <th>{{ __('users.role') }}</th>
                  <!-- <th>{{ __('users.gender') }}</th> -->
                  <th>{{ __('users.email') }}</th>
                  <th>{{ __('users.active') }}</th>
                  <th>{{ __('users.created_by') }}</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                <tr>
                  <!-- <td>{{ $user->avatar }}</td> -->
                  <td>{{ $user->fullname }}</td>
                  <td>{{ $user->username }}</td>
                  <!-- <td style="width:120px">
                      @foreach (arr_permission($user->id) as $key)
                        <li>{{  $key->name }}.</li>
                      @endforeach
                  </td> -->
                  <td>
                    @foreach (arr_role($user->id) as $key)
                      {{  $key->name }} <br/>
                    @endforeach
                  </td>
                  <!-- <td>
                  @if ($user->gender == 1)
                      Nam
                  @elseif ($user->gender == 0)
                      Nữ
                  @else 
                    @if (empty($user->gender))
                      Chưa cập nhật
                    @endif
                  @endif
                  </td> -->
                  <td>{{ $user->email }}</td>
                  
                  <td style="font-size:18px">
                  @if($user->active =='1')
                    <span class="badge bg-success">{{ __('users.active_status') }}</span>
                  @else
                    <span class="badge bg-danger" data-toggle="tooltip" title="{{ __('users.active_now') }}">
                    <a style="text-decoration:none; cursor: pointer;" onclick="activeStatus({{$user->id}})"  data-toggle="modal" data-target="#active">
                    {{ __('users.not_active') }}
                    </a>
                    </span>
                    <div class="modal fade" id="active"  role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body " style="background-image: url({{ asset('dist/img/agree.png') }}); background-repeat: no-repeat; background-size: 130px; padding-left: 130px;">
                          <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('users.user_activated') }}</h3>
                          <form method="post" id="activeForm" action="">
                              @csrf
                              @method('PUT')
                              <input type="hidden" name="id" id="id">
                              <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                              <i class="fa fa-times" aria-hidden="true"></i>
                                  {{ __('users.cancel') }}
                              </button>
                              <button type="submit" name="" class="btn btn-success" data-dismiss="modal" onclick="formActiveSubmit()">
                              <i class="fa fa-check" aria-hidden="true"></i>
                                {{ __('users.btn_active') }}
                              </button>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  @endif
                  </td>
                  <td>
                    @if (isset($user->created_by))
                    {{ user_name($user->created_by) }}
                    @else
                     {{ __('users.self_created_system') }}
                    @endif
                  </td>
                  <td>
                  <a href="{{ route('user.show', $user->id) }}" > 
                      <i data-toggle="tooltip" title="{{ __('users.tooltip_view') }}" style="text-align: right;" class="fa fa-eye" aria-hidden="true"></i>
                  </a>| 
                    <a href="{{ route('user.edit',$user->id) }}"> 
                      <i data-toggle="tooltip" title="{{ __('users.tooltip_edit') }}" style="text-align: right;" class="	fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>  
                    | 
                    <a href="javascript(0);" onclick="deleteData({{$user->id}})"  style="color:red" data-toggle="modal" data-target="#confirm" > 
                      <i data-toggle="tooltip" title="{{ __('users.tooltip_delete') }}" style="text-align: right;" class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                    <div class="modal fade" id="confirm"  role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body " style="background-image: url({{ asset('dist/img/delete.jpg') }}); background-repeat: no-repeat; background-size: 130px; padding-left: 130px;">
                          <h3 class="card-title modal-title w-100" style="margin-bottom: 20px;">{{ __('users.user_delete') }}</h3>
                          <form method="post" id="deleteForm" action="">
                              @csrf
                              @method('DELETE')
                              <input type="hidden" name="id" id="id">
                              <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                              <i class="fa fa-times" aria-hidden="true"></i>
                                  {{ __('users.cancel') }}
                              </button>
                              <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                                {{ __('users.delete') }}
                              </button>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@include('user.create')
<!-- /.modal -->

@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@push('js')
<script type="text/javascript">
  function deleteData(id)
   {
      var id = id;
      var url = '{{ route("user.destroy", ":id") }}';
      url = url.replace(':id', id);
      $("#id").val(id);
      $("#deleteForm").attr('action', url);
    }

    function formSubmit()
    {
      $("#deleteForm").submit();
    }

  function activeStatus(id)
   {
      var id = id;
      var url = '{{ route("activeStatus", ":id") }}';
      url = url.replace(':id', id);
      $("#id").val(id);
      $("#activeForm").attr('action', url);
    }

     function formActiveSubmit()
     {
        $("#activeForm").submit();
     }
     
</script>
@endpush

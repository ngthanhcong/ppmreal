@extends('layouts.master')
@section('title')
 {{ __('Thông tin cá nhân')  }}
@stop

@php
  function arr_permission($user_id)
  {
    $permissions = App\User::join('users_permissions', 'users.id', '=', 'users_permissions.user_id')
            ->join('permissions', 'users_permissions.permission_id', '=', 'permissions.id')
            ->where('users.id', $user_id)
            ->select('permissions.name')
            ->get();

    return $permissions;
  }
  function arr_role($user_id)
  {
    $roles = App\User::join('users_roles', 'users.id', '=', 'users_roles.user_id')
            ->join('roles', 'users_roles.role_id', '=', 'roles.id')
            ->where('users.id', $user_id)
            ->select('roles.name')
            ->get();

    return $roles;
  }

@endphp
<style>
.img-round {
  position: relative;
  height: 0;
  padding-bottom: 100%;
  overflow: hidden;
  border-radius: 50%;
}
.img-round > img {
  position: absolute;
  width: 100%;
  height: 100%;
  -o-object-fit: cover;
     object-fit: cover;
}
.datepicker{
    z-index: 1100 !important;
}
</style>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1>{{ __('Thông tin cá nhân')}}</h1> -->
          </div>
          @if (Auth::user()->hasRole('Admin'))
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.profile') }}</li>
            </ol>
          </div>
          @endif
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
             <h3 class="card-title">{{ __('users.profile')}}</h3>
            </div>  
            <!-- /.card-header -->
            <div class="card-body">
                <div class="form-group" style="width:auto; margin-top: -15px; margin-bottom: -10px;" >
                    <button type="button" class="btn btn-outline-secondary">
                        <a style="text-decoration: none; color: black;" href="{{ redirect()->getUrlGenerator()->previous() }}">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('users.back') }}
                        </a>
                    </button>
                </div>
                <hr />
            <div class="container">
                <div class="row">
					        <div style="col-sm">
                      <!-- <div class='col-sm-7' >
                        <div class='img-round'>
                                @if (empty($product->avatar))
                                    <img id="avatar_show" src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                    <br> 
                                @else
                                    <img id="avatar_show" src="{{ url($product->avatar)}}" />
                                @endif
                        </div>
                        <br> 
                        <input onchange="readURL(this);" name="avatar" type="file" id="avatar_upload"/>  
                    </div> -->
					        </div>
                    <div class="col-sm">
                        <div class="form-group">
                          <b>{{ __('users.fullname') }}: </b>
                                        {{$user->fullname}}
                        </div>
                        <div class="form-group">
                          <b>{{ __('users.username') }}: </b> 								
                                        {{$user->username}}
                          </div>
                        <div class="form-group">
                          <b>{{ __('users.gender') }}: </b>
                            @if ($user->gender == 1)
                                Nam
                            @elseif ($user->gender == 2)
                                Nữ
                            @elseif ($user->gender == null)
                                {{ __('users.not_update')}}
                            @endif
                            </div>
                            <div class="form-group">
                              <b>{{ __('users.birthday') }}: </b>
                                            {{ date('d/m/Y', strtotime($user->birthday)) }}
                            </div>

                            <div class="form-group" >
                              <b>{{ __('users.phone') }}: </b>
                                            @if($user->phone ==NULL)
                                                {{ __('users.not_update')}}
                                            @else
                                                {{$user->phone}}
                                            @endif
                            </div>

                        <div class="form-group" >
                          <b>{{ __('users.email') }}: </b>
                          {{$user->email}}
                        </div>

                                    <div class="form-group" >
                          <b>{{ __('users.address') }}: </b>
                                        @if($user->phone ==NULL)
                                            {{ __('users.not_update')}}
                                        @else
                                            {{$user->address}}
                                        @endif
                                    </div>
                            </div>
                            <div class="col-sm">
                                    <div class="form-group" >
                          <b>{{ __('users.permission') }}: </b>
                                        <ul>
                          @foreach (arr_permission($user->id) as $key)
                                        <li>{{  $key->name }}.</li>
                                        @endforeach
                                        </ul>
                                    </div>

                                    <div class="form-group" >
                          <b>{{ __('users.role') }}: </b>
                          @foreach (arr_role($user->id) as $key)
                                        {{  $key->name }} <br/>
                                        @endforeach
						                 </div>
                          </div>
                        </div>
                    <div class="form-group" style="position:center">
                            <button type="button" data-toggle="modal" data-target="#modalUpdateForm" type="button" class="btn btn-outline-secondary ">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ __('users.update_infomation') }}
                            </button>
						        </div>
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
<!-- Modal update profile-->
<div class="modal fade" id="modalUpdateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header card-primary">
            <!-- <h4 class="modal-title w-100 font-weight-bold">Sign up</h4> -->
            <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('users.update_infomation') }}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
      <form method="post" action="{{ route('profileUpdate', $user->id) }}">
                  @csrf
                  @method('PUT')
                  <div class="container">
                    <table>
                    <div class="row">
                      <tr class="col-md-6" >
                        <td ><b>Tên đăng nhập</b></td>
                        <td  style="padding:5px"> <input readonly id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{$user->username}}" autofocus></td>
                      </tr>
                      <tr class="col-md-6" >
                        <td ><b>Tên đầy đủ</b></td>
                        <td style="padding:5px"><input id="fullname" type="text" class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" name="fullname" value="{{$user->fullname}}" autofocus></td>
                      </tr>
                      <tr class="col-md-6" >
                        <td ><b>Địa chỉ</b></td>
                        <td style="padding:5px"><input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{$user->address}}" autofocus></td>
                      </tr>
                      <tr class="col-md-6" >
                        <td ><b>Email</b></td>
                        <td style="padding:5px"><input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$user->email}}" autofocus></td>
                      </tr>
                      <tr class="col-md-6" >
                        <td ><b>Ngày sinh</b></td>
                        <td style="padding:5px">
                        <input id="birthday" type="text" class="form-control datepicker {{ $errors->has('birthday') ? ' is-invalid' : '' }}" name="birthday" value="{{ old('birthday', date('d/m/Y', strtotime($user->birthday))) }}" autofocus></td>
                      </tr>
                      <tr class="col-md-6" >
                        <td ><b>Giới tính</b></td>
                        <td style="padding:5px">  
                            <input name="gender" value="1" type="radio"  @if ( old('gender', $user->gender) == 1) checked @endif >Nam
                            <input name="gender" value="2" type="radio" @if ( old('gender', $user->gender) == 2) checked @endif >Nữ</td>
                      </tr>
                      <tr class="col-md-6" >
                        <td ><b>Số điện thoại</b></td>
                        <td style="padding:5px"><input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{$user->phone}}" autofocus></td>
                      </tr>
                      </div>
                    </table>
                  </div> 

                  <div style="text-align: center;"> 
                      <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
                        <i class="fa fa-times" aria-hidden="true"></i>{{ __('users.cancel') }}
                      </button>
                      <button type="submit" class="btn btn-success">
                        <i class="fa fa-save" aria-hidden="true"></i> {{ __('users.save') }}
                      </button>
                    </div>
                </form>
    </div>
  </div>
</div>

<!-- ./modal update profile -->
@endsection
@push('js')
<!-- Load img before update -->
<script src="{{ asset('dist/js/plugins/jquery/jquery.js') }}"></script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#avatar_show').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar_upload").change(function(){
        readURL(this);
    });
</script>
@if (Session::has('message'))
<script type="text/javascript">
    jQuery("body").load(window.location.href);
</script>
@else
@endif
@endpush
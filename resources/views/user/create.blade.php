<div class="modal fade" id="modalCreateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header card-primary">
            <!-- <h4 class="modal-title w-100 font-weight-bold">Sign up</h4> -->
            <h3 class="card-title modal-title w-100 font-weight-bold">{{ __('users.user_create') }}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body mx-3">
      <form method="post" action="{{ route('user.store') }}">
        @csrf
        @method('POST')
        <div class="md-form mb-3">
            <label for="username">{{ __('users.username') }} <sup class="text-danger">(*)</sup></label>
            <input required id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" autofocus>
        </div>
        <div class="md-form mb-3">
            <label for="fullname">{{ __('users.fullname') }} <sup class="text-danger">(*)</sup></label>
            <input required id="fullname" type="text" class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" name="fullname" value="{{ old('fullname') }}" autofocus>
        </div>
        <div class="md-form mb-3">
            <label for="pass_first">{{ __('users.password') }} <sup class="text-danger">(*)</sup></label>
            <input required id="pass_first" type="password" class="form-control{{ $errors->has('pass_first') ? ' is-invalid' : '' }}" name="pass_first" value="{{ old('pass_first') }}" autofocus>
       </div> 
       <div class="md-form mb-3">
            <label for="id_role">{{ __('users.role') }} <sup class="text-danger">(*)</sup></label>
            <select id="id_role" type="text" class="form-control{{ $errors->has('id_role') ? ' is-invalid' : '' }}" name="id_role" autofocus>
              @foreach($role as $role)
              <option required value="{{$role->id}}">{{$role->slug}}</option>
              @endforeach
            </select>
       </div> 
       <div class="md-form mb-3" >
            <label for="id_per">{{ __('users.permission') }} <sup class="text-danger">(*)</sup></label>
            <br>
            @foreach($per as $per)
             <label class="checkbox-inline" >
              <input type="checkbox" name="id_per[]" value="{{$per->id}}" style="width:45px">{{$per->name}}
            </label>
            @endforeach
       </div> 
        <div class="md-form mb-3">
            <label for="active">{{ __('users.active') }} <sup class="text-danger">(*)</sup></label>
            <input required name="active" type="radio" value="1"/> {{ __('users.active_status')}}
            <input required name="active" type="radio" value="0"/> {{ __('users.not_active')}}
        </div>
      <div class="md-form mb-3">
            <label for="email">{{ __('users.email') }} <sup class="text-danger">(*)</sup></label>
            <input required id="email" type="email"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus>
        </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-save" aria-hidden="true"></i>
            {{ __('users.save') }}
        </button>
        <button data-dismiss="modal" aria-label="Close" class="btn btn-outline-secondary">
            <i class="fa fa-times" aria-hidden="true"></i>
            {{ __('users.cancel') }}
        </button>
      </div>
      </form>
    </div>
  </div>
</div>

@extends('layouts.master')
@section('title')
  {{ __('users.edit_users') }}
@stop

@php
function arr_permission($user_id)
  {
    $permissions = App\User::join('users_permissions', 'users.id', '=', 'users_permissions.user_id')
            ->join('permissions', 'users_permissions.permission_id', '=', 'permissions.id')
            ->where('users.id', $user_id)
            ->get();

    return $permissions;
  }
@endphp
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('users.user_management')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('users.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('users.user') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
             <h3 class="card-title">{{ __('users.edit_users') }}: {{$user->fullname}}</h3>
            </div>  
            <!-- /.card-header -->
            <div class="card-body">
                <form method="post" action="{{ route('user.update', $user->id) }}">
                  @csrf
                  @method('PUT')
                  <div class="form-group" style="width:auto; margin-top: -15px; margin-bottom: -10px;" >
                    <button type="button" class="btn btn-outline-secondary">
                        <a style="text-decoration: none; color: black;" href="{{ route('user.index') }}">
                          <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('users.back') }}
                        </a>
                    </button>
                    <div style="right: 0; margin-top: -35px; margin-bottom: -10px; text-align: right;"> 
                      <button type="reset" class="btn btn-outline-secondary">
                        <i class="fa fa-times" aria-hidden="true"></i>{{ __('users.cancel') }}
                      </button>
                      <button type="submit" class="btn btn-success">
                        <i class="fa fa-save" aria-hidden="true"></i> {{ __('users.save') }}
                      </button>
                    </div>
                  </div>
                  <hr />
                  <div class="container" style="margin-left: -10px;">
                    <div class="row">
                      <div class="col">
                        <h3 class="card-title">{{ __('users.profile')}}</h3>
                        <table style="margin-left:20px; width:700px;">
                      <tr>
                        <td style="width:150px;padding:5px"><b>{{ __('users.username')}}</b></td>
                        <td  style="padding:10px"> <input readonly id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{$user->username}}" autofocus></td>
                      </tr>
                      <tr>
                        <td style="width:150px;padding:5px"><b>{{ __('users.fullname')}}</b></td>
                        <td style="padding:10px"><input id="fullname" type="text" class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" name="fullname" value="{{$user->fullname}}" autofocus></td>
                      </tr>
                      <!-- <tr>
                        <td style="width:150px;padding:5px"><b>Mật khẩu</b></td>
                        <td style="padding:10px"><input id="pass_first" type="text" class="form-control{{ $errors->has('pass_first') ? ' is-invalid' : '' }}" name="pass_first" value="{{$user->pass_first}}" autofocus></td></td>
                      </tr> -->
                      <tr>
                        <td style="width:150px;padding:5px"><b>{{ __('users.active')}}</b></td>
                        <td style="padding:10px"> 
                         <input name="active" value="1" type="radio"  @if (old('active', $user->active) == '1') checked @endif/>{{ __('users.active_status')}}
                         <input name="active"  value="0" type="radio" @if (old('active', $user->active) == '0') checked @endif/>{{ __('users.not_active')}}
                         </td>
                      </tr>
                      <tr>
                        <td style="width:150px;padding:5px"><b>{{ __('users.address')}}</b></td>
                        <td style="padding:10px"><input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{$user->address}}" autofocus></td>
                      </tr>
                      <tr>
                        <td style="width:150px;padding:5px"><b>{{ __('users.email')}}</b></td>
                        <td style="padding:10px"><input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$user->email}}" autofocus></td>
                      </tr>
                      <tr>
                        <td style="width:150px;padding:5px"><b>{{ __('users.birthday')}}</b></td>
                        <td style="padding:10px"><input id="birthday" type="text" class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }}" name="birthday" value="{{ old('birthday', date('d/m/Y', strtotime($user->birthday))) }}" autofocus></td>
                      </tr>
                      <tr>
                        <td style="width:150px;padding:5px"><b>{{ __('users.gender')}}</b></td>
                        <td style="padding:10px">  
                          <input name="gender" value="1" type="radio"  @if (old('gender', $user->gender) == '1') checked @endif/>Nam
                          <input name="gender"  value="0" type="radio" @if (old('gender', $user->gender) == '0') checked @endif/>Nữ
                        </td>
                      </tr>
                      <tr>
                        <td style="width:150px;padding:5px"><b>{{ __('users.phone')}}</b></td>
                        <td style="padding:10px"><input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{$user->phone}}" autofocus></td>
                      </tr>
                    </table>
                      </div>
                      <div class="col">
                        <h3 class="card-title">{{ __('users.permission_users')}}:</h3>
                          <div class="md-form mb-3">
                            <label for="id_per">{{ __('users.permission') }}</label>
                            <br>
                            @foreach($per as $per)
                              <label class="checkbox-inline" >
                                <input type="checkbox" name="id_per[]" value="{{ $per->id }}" 
                                {{ $user->can($per->slug) ? "checked" : "" }}
                                style="width:45px">{{$per->name}}
                              </label>
                            @endforeach
                          </div> 
                      </div>
                    </div>
                    
                  </div> 
                </form>
            </div>
            <!-- /.card-body -->
        </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush
@push('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

@endpush


<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emp_role = Role::where('slug','employees')->first();
        $admin_role = Role::where('slug', 'Admin')->first();
        $admin_create_perm = Permission::where('slug','create')->first();
        $admin_delete_perm = Permission::where('slug','delete')->first();
        $admin_edit_perm = Permission::where('slug','edit')->first();
        $admin_perm_list = Permission::where('slug','view-all')->first();
        $admin_perm = Permission::where('slug','view')->first();
        $emp_edit = Permission::where('slug','edit-profiles')->first();
        $emp_view = Permission::where('slug','view-profiles')->first();
        $emp_create_user_sale = Permission::where('slug','create-user-sale')->first();

        $emp_create = new User();
        $emp_create->fullname = 'I am Employees';
        $emp_create->username = 'Employees';
        $emp_create->email = 'Emplpyees@test.com';
        $emp_create->pass_first ='123456';bcrypt('secret');
        $emp_create->password =bcrypt('123456');
        $emp_create->phone = '0965123456';
        $emp_create->active='1';
        $emp_create->save();
        $emp_create->roles()->attach($emp_role);
        $emp_create->permissions()->attach($emp_edit);
        $emp_create->permissions()->attach($emp_view);
 
       
        $Admin = new User();
        $Admin->fullname = 'I am admin';
        $Admin->username = 'Administrator';
        $Admin->email = 'admin@test.com';
        $Admin->pass_first ='123456';
        $Admin->password =bcrypt('123456');
        $Admin->active='1';
        $Admin->phone = '0965123456';
        $Admin->save();
        $Admin->roles()->attach($admin_role);
        $Admin->permissions()->attach( $admin_create_perm);
        $Admin->permissions()->attach( $admin_delete_perm);
        $Admin->permissions()->attach( $admin_edit_perm);
        $Admin->permissions()->attach( $admin_perm_list);
        $Admin->permissions()->attach( $admin_perm);
        $Admin->permissions()->attach( $emp_edit);
        $Admin->permissions()->attach( $emp_view);
        $Admin->permissions()->attach( $emp_create_user_sale);

    }
}

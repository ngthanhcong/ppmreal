<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fullname'  => 'Admin',
            'username'  =>  'admin ',
            'password'      => bcrypt('123456'),
            'pass_first'   => '123456',
            'remember_token' => '',
            'avatar'        => '',        
            'gender'        => '1',
            'birthday'      => '1990-03-07 15:47:15',
            'phone'     => '0965123456',
            'email'     => 'cong@gmail.com',
            'address'       => 'Xuân Khánh, Ninh Kiền, Cần Thơ',
            'active'        => '1',
            'attribs'       => '',
            'last_login'    => date('Y-m-d h:m:s'),
            'note'          => '',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
    }
}

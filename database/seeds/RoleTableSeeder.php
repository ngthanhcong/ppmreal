<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'slug'  => 'Employees',
            'name'  =>  'Employees',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);

        DB::table('roles')->insert([
            'slug'  => 'Admin',
            'name'  =>  'Administator',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
    }
}

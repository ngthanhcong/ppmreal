<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Tạo tài khoản người dùng
        DB::table('permissions')->insert([
            'slug'  => 'create',
            'name'  =>  'Tạo mới',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
        //Xóa tài khoản ngưởi dùng
        DB::table('Permissions')->insert([
            'slug'  => 'delete',
            'name'  =>  'Xóa',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
        //Xem thông tin cá nhân người dùng
        DB::table('Permissions')->insert([
            'slug'  => 'view',
            'name'  =>  'Xem chi tiết',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
        //Chỉnh sửa ngưởi dùng
        DB::table('Permissions')->insert([
            'slug'  => 'edit',
            'name'  =>  'Chỉnh sửa',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
        //Xem ds người dùng
        DB::table('Permissions')->insert([
            'slug'  => 'view-all',
            'name'  =>  'Xem tất cả',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
        //Xem thông tin cá nhân
        DB::table('Permissions')->insert([
            'slug'  => 'view-profile',
            'name'  =>  'Xem thông tin cá nhân',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
        //Chỉnh sửa thông tin cá nhân
        DB::table('Permissions')->insert([
            'slug'  => 'edit-profiles',
            'name'  =>  'Chỉnh sửa thông tin cá nhân',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);
        //Tạo mới nhân viên cập dưới
        DB::table('Permissions')->insert([
            'slug'  => 'create-user-sale',
            'name'  =>  'Tạo mới nhân viên cấp dưới',
            'created_at'     => date('Y-m-d h:m:s'),
            'updated_at'     => date('Y-m-d h:m:s'),
        ]);

    }
}

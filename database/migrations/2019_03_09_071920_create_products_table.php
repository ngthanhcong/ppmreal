<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('ward_id')->comment('Phường');
            $table->unsignedInteger('user_id')->comment('Người tạo');
            $table->string('product_name')->comment('Tên sản phẩm');
            $table->string('alias');
            $table->text('avatar')->comment('Ảnh đại diện');
            $table->string('alt')->nullable()->comment('alt cho ảnh đại diện');
            $table->text('images')->nullable()->comment('Mảng gồm vị trí và alt cho mỗi ảnh');
            $table->text('summary')->nullable()->comment('Tóm tắt');
            $table->text('content')->nullable()->comment('Nội dung');
            $table->decimal('price', 15, 0)->nullable()->comment('Giá');
            $table->string('acreage')->comment('Diện tích');
            $table->longText('area')->nullable()->comment('Mảng các điểm mốc của sản phẩm');
            $table->double('length', 15, 0)->nullable()->comment('Chiều dài');
            $table->double('width', 15, 0)->nullable()->comment('Chiều rộng');
            $table->string('direction')->comment('Hướng nhà');
            $table->tinyInteger('status')->comment('Trạng thái');
            $table->longText('lat')->nullable();
            $table->longText('lng')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('ward_id')->references('id')->on('wards')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

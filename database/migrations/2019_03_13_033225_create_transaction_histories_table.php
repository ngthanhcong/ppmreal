<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('Nhân viên giao dịch');
            $table->unsignedInteger('customer_id')->comment('Khách hàng giao dịch');
            $table->unsignedInteger('product_id')->comment('Sản phẩm giao dịch');
            $table->tinyInteger('transaction_status')->comment('Trạng thái giao dịch');
            $table->timestamp('transactioned_at')->comment('Thời gian giao dịch');
            $table->decimal('deposit_amount', 15, 0)->comment('Số tiền đặt cọc')->nullable();
            $table->boolean('pay_status')->comment('Trạng thái thanh toán');
            $table->timestamp('paid_at')->nullable()->comment('Thời gian thanh toán');
            $table->tinyInteger('paid_by')->comment('Hình thức thanh toán');
            $table->string('note')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_histories');
    }
}

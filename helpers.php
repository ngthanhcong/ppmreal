<?php

/* get current uri without prefix */
function uri($path = ''){
    $route = '/' . Request::path();
    $prefix = Route::current()->getPrefix() . '/';
    return explode($prefix, $route)[1] . ($path == '' ? '' : '/'.$path);
}
/* get current request URI */
function request_uri(){
    return Request::path();
}
function price_to_str($value, $level, $lang){
    /*Format price with EN*/
    if($lang == 'en'){
        return number_format($value, 0, ".", ",");
    }
        
    /*Initialize*/
    $digits = strlen($value);
    if($digits > 15)
        return $value;
    $class = trans('product.price-conversion')[$digits];
    $result = '';
    
    /*Convert with VI only*/
    if($level < 3){
        switch($digits){
            case 3:
                $result = substr(ltrim($value, '0'), 0, -2) . ' ' . $class;
                break;
            case 4:
            case 5:
            case 6:
                $result = substr(ltrim($value, '0'), 0, -3) . ' ' . $class;
                if($value[$digits-3] != 0){
                    $result .= ' ' . price_to_str(substr(ltrim($value, '0'), -3), ++$level, $lang);
                }
                break;
            case 7:
            case 8:
            case 9:
                $result = substr(ltrim($value, '0'), 0, -6) . ' ' . $class;
                if( ($value[$digits-6] != 0 - $value[$digits-5] - $value[$digits-4]) != 0){
                    $result .= ' ' . price_to_str(substr(ltrim($value, '0'), -6), ++$level, $lang);
                }
                break;
            case 10:
            case 11:
            case 12:
                $result = substr(ltrim($value, '0'), 0, -9) . ' ' . $class;
                if( ($value[$digits-9] != 0 - $value[$digits-8] - $value[$digits-7]) != 0){
                    $result .= ' ' . price_to_str(substr(ltrim($value, '0'), -9), ++$level, $lang);
                }
                break;
            case 13:
            case 14:
            case 15:
                $result = substr(ltrim($value, '0'), 0, -12) . ' ' . $class;
                break;
            default:
                $result = $value;
                break;
        }
        return $result;
    }
}
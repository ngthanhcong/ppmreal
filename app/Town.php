<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{

    protected $fillable = [
        'town_name',
        'areas',
        'note'
    ];
    public $rules = [
        'town_name'     => 'required',
        'areas'         => 'required',
        'note'          => ''
    ];
    public function districts()
    {
        return $this->hasMany('App\District', 'town_id');
    }

}

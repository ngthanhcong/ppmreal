<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Providers\PermissionsServiceProvider;

class Permission extends Model
{
    protected $fillable = [
        'slug',
        'name', 
        'created_at',
        'updated_at'
    ];
}

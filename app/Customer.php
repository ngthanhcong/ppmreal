<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $fillable = [
        'customer_name',
        'customer_address',
        'customer_gender',
     	'customer_phone',
        'customer_email',
        'customer_note',
    ];
    // public $rules = [
    //     'fullname' => '',
    //     'address' => '',
    //     'gender' => '',
    //  	'phone' => '',
    //     'email' => '',
    // ];

}





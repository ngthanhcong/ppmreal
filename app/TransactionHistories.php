<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionHistories extends Model
{
    protected $fillable = [
    'user_id',
    'customer_id',
    'product_id',
    'transaction_status',
    'transactioned_at',
    'deposit_amount',
    'pay_status',
    'paid_at',
    'paid_by',
    'note',
    ];
}

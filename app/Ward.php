<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{

    protected $fillable = [
        'district_id',
        'ward_name',
        'areas',
        'note'
    ];
    public $rules = [
        'district_id'   => '',
        'ward_name'     => 'required',
        'areas'         => 'required',
        'note'          => ''
    ];
    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }

}

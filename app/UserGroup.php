<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model {

    protected $fillable = [
        'group_name',
        'note',
    ];
    public $rules       = [
        'group_name' => 'required|unique:user_groups,group_name',
        'note'       => '',
    ];

}

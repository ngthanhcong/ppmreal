<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'category_id',
        'ward_id',
        'user_id',
        'product_name',
        'alias',
        'avatar',
        'alt',
        'images',
        'summary',
        'content',
        'price',
        'acreage',
        'area',
        'length',
        'width',
        'direction',
        'status',
        'lat',
        'lng',
        'note',
            
    ];
    
}

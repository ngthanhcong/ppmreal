<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $fillable = [
    'user_id',
    'customer_id',
    'product_id',
    'consulting_status',
    'consulting_at',
    'note',
    ];
}

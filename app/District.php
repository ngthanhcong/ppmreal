<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{

    protected $fillable = [
        'town_id',
        'district_name',
        'areas',
        'note'
    ];
    public $rules = [
        'town_id'       => '',
        'district_name' => 'required',
        'areas'         => 'required',
        'note'          => ''
    ];
    public function town()
    {
        return $this->belongsTo('App\Town', 'town_id');
    }
    public function wards()
    {
        return $this->hasMany('App\Ward', 'district_id');
    }

}

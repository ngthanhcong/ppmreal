<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProduct;
use App\Product;
use App\History;
use Carbon\Carbon;
use Auth;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->user()->can('view-all')) {
            $products = Product::get();
            return view('product.index',compact('products', $products));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->user()->can('create')) {
        return view('product.create');
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->can('create')) {
         // validation
        $messages = [
            'category_id.required'  => __('product.category_id') .' là bắt buộc',
            'ward_id.required'      => __('product.ward_id') . ' là bắt buộc',
            // 'user_id.required'      => __('product.user_id') . ' là bắt buộc',
            'product_name.required' => __('product.product_name') . ' là bắt buộc',
            'alias.required'        => __('product.alias') . ' là bắt buộc',
            'acreage.required'      => __('product.acreage') . ' là bắt buộc',
            'direction.required'    => __('product.acreage') . ' là bắt buộc',
            'status.required'       => __('product.status') . ' là bắt buộc',
        ];
        $validator = Validator::make($request->all(), [
            'category_id'       => 'required',
            'ward_id'           => 'required',
            // 'user_id'           => 'required',
            'product_name'      => 'required|max:191',
            'alias'             => 'required',
            'avatar'            => '',
            'alt'               => '',
            'images'            => '',
            'summary'           => '',
            'content'           => '',
            'price'             => '',
            'acreage'           => 'required',
            'area'              => '',
            'length'            => '',
            'width'             => '',
            'direction'         => 'required',
            'status'            => 'required',
            'lat'               => '',
            'lng'               => '',
            'note'              => '',
        ], $messages );
        
        if ($validator->fails()) {
            return redirect('product/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        // and validation
        // $error_price = $request->price;
        //validation price
        // if ( $error_price <= 1000000 || $error_price <0 || $error_price >=100000000000000 )
        // {
        //     $notification = array(
        //         'message' => 'Giá trị phải >=1.000.000 và lớn hơn 0 và <=100.000.000.000.000!',
        //         'alert-type' => 'error'
        //     );
        //     return redirect('product/create')->with($notification);
        // }
        // end validation price
        // check product_name and $request->file('avatar') and create Product
        $error = Product::where('product_name', $request->product_name)->first();
        
        if(isset($error_avatar))
        {
            $notification = array(
                'message' => 'Ảnh đại diện đã tồn tại!',
                'alert-type' => 'error'
            );
        } 
        if (isset($error))
        {
            $notification = array(
                'message' => 'Tên vừa nhập đã tồn tại!',
                'alert-type' => 'error'
            );
        }
        else
        {
            //check $request->file('avatar')
            $avatar = $request->file('avatar');
            // $avatar_add = 'uploads/images/' . $avatar->getClientOriginalName();
            // $error_avatar = Product::where('avatar', $avatar_add)->first();
            if (empty($avatar))
            {
                $avatar_add = '';
            }
            else
            {
                $destinationPath = base_path() . '/public/uploads/images/';
                $avatar->move($destinationPath, $avatar->getClientOriginalName());
                $avatar_add = 'uploads/images/' . $avatar->getClientOriginalName();
            }
            //end check $request->file('avatar')
            //create product
            $product = Product::create([
                'category_id'       => $request->category_id,
                'ward_id'           => $request->ward_id,
                'user_id'           => Auth::id(),
                'product_name'      => $request->product_name,
                'alias'             => $request->alias,
                'avatar'            => $avatar_add,
                'alt'               => $request->alt,
                'images'            => $request->images,
                'summary'           => $request->summary,
                'content'           => $request->content,
                'price'             => $request->price,
                'acreage'           => $request->acreage,
                'area'              => $request->area,
                'length'            => $request->length,
                'width'             => $request->width,
                'direction'         => $request->direction,
                'status'            => $request->status,
                'lat'               => $request->lat,
                'lng'               => $request->lng,
                'note'              => $request->note,
            ]);
            //end create 
            $notification = array(
                'message' => 'Thêm mới thành công!',
                'alert-type' => 'success'
            );
           
        }
        return redirect('/product')->with($notification);
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request, $alias)
    {
        if ($request->user()->can('view')) {
        $product = Product::where('alias', $alias)->first();
        return view('product.show', compact('product'));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->user()->can('edit')) 
        {
            $product = Product::where('id', $id)->first();
            if ($product->status != 1)
            {
                $notification_error = array(
                    'message' => 'Vui lòng không chỉnh sửa thông tin khi mảnh đất đã bán hoặc đang tư vấn!',
                    'alert-type' => 'error'
                );
                return back()->with($notification_error);
            }
            else
            {
                return view('product.edit', compact('product'));
            }
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->can('edit')) 
        {
        // validation
        $messages = [
            'category_id.required'  => __('product.category_id') .' là bắt buộc',
            'ward_id.required'      => __('product.ward_id') . ' là bắt buộc',
            // 'user_id.required'      => __('product.user_id') . ' là bắt buộc',
            'product_name.required' => __('product.product_name') . ' là bắt buộc',
            'alias.required'        => __('product.alias') . ' là bắt buộc',
            'acreage.required'      => __('product.acreage') . ' là bắt buộc',
            'direction.required'    => __('product.direction') . ' là bắt buộc',
            'status.required'       => __('product.status') . ' là bắt buộc',
        ];
        $validator = Validator::make($request->all(), [
            'category_id'       => 'required',
            'ward_id'           => 'required',
            // 'user_id'           => 'required',
            'product_name'      => 'required|max:191',
            'alias'             => 'required',
            'avatar'            => '',
            'alt'               => '',
            'images'            => '',
            'summary'           => '',
            'content'           => '',
            'price'             => '',
            'acreage'           => 'required',
            'area'              => '',
            'length'            => '',
            'width'             => '',
            'direction'         => 'required',
            'status'            => 'required',
            'lat'               => '',
            'lng'               => '',
            'note'              => '',
        ], $messages );
        
        if ($validator->fails()) {
            dd($validator);
            return redirect('product/' + '$id' +'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
        // end validation
        // check empty $product->area; $product->lat; $product->lng;
        $product = Product::where('id', $id)->first();
        if (empty($request->area))
        {
            $area = $product->area;
            $lat = $product->lat;
            $lng = $product->lng;
        }
        else
        {
            $area = $request->area;
            $lat = $request->lat;
            $lng = $request->lng;
        }
        // end check 
        // check avatar
        $avatar = $request->file('avatar');
            if (empty($avatar))
            {
                $avatar_update = $product->avatar;
            }
            else
            {
                $destinationPath = base_path() . '/public/uploads/images/';
                $avatar->move($destinationPath, $avatar->getClientOriginalName());
                $avatar_update = 'uploads/images/' . $avatar->getClientOriginalName();
            }
        // end avatar
        // update
        $product->update([
            'category_id'       => $request->category_id,
            'ward_id'           => $request->ward_id,
            'user_id'           => Auth::id(),
            'product_name'      => $request->product_name,
            'alias'             => $request->alias,
            'avatar'            => $avatar_update,
            'alt'               => $request->alt,
            'images'            => $request->images,
            'summary'           => $request->summary,
            'content'           => $request->content,
            'price'             => $request->price,
            'acreage'           => $request->acreage,
            'area'              => $area,
            'length'            => $request->length,
            'width'             => $request->width,
            'direction'         => $request->direction,
            'status'            => $request->status,
            'lat'               => $lat,
            'lng'               => $lng,
            'note'              => $request->note,
        ]);
        // end update
        // notification
        $notification = array(
            'message' => 'Cập nhật thành công!',
            'alert-type' => 'success'
        );
        return redirect('/product')->with($notification);
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->user()->can('delete')) 
        {
        $product = Product::where('id', $id)->first();
        $error_delete = History::where([
            ['product_id', $id],
            ['consulting_status', 1]
            ])->get();
        if (isset($error_delete) && ($product->status == 2))
        {
            $notification_error = array(
                'message' => 'Thao tác thất bại! Mảnh đất đang được tư vấn hoặc đã bán.',
                'alert-type' => 'error'
            );
            return back()->with($notification_error);
        }
        else
        {
            $product->delete();
            $notification = array(
                'message' => 'Xóa bỏ thành công!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
            }
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }
}

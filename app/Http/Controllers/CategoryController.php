<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCategory;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->can('view-all')) 
        {
            $categories = Category::get();
            return view('category.index', compact('categories'));
        } 
            $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
            );
        return  back()->with($notification_error);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->user()->can('create')) {
        return view('category.create');
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategory $request)
    {
        if ($request->user()->can('create')) {
        $error = Category::where('category_name', $request->category_name)->first();
        if (isset($error))
        {
            $notification = array(
                'message' => 'Tên danh mục đã tồn tại!',
                'alert-type' => 'error'
            );
        }
        else
        {
            $cat = Category::create($request->all());
            $notification = array(
                'message' => 'Thêm mới thành công!',
                'alert-type' => 'success'
            );
           
        }
        return back()->with($notification);
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_error);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->user()->can('edit')) {
        return view('category.edit');
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->can('edit')) {
        $cat = Category::where('id', $id)->first();
        $cat->update($request->all());
        $notification = array(
            'message' => 'Cập nhật thành công!',
            'alert-type' => 'success'
        );
        return back()->with($notification);
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function destroy(Request $request, $id)
    {
        if ($request->user()->can('delete')) {
        $cat = Category::where('id', $id)->first();
        $cat->delete();
        $notification = array(
            'message' => 'Xóa bỏ thành công!',
            'alert-type' => 'success'
        );
        return back()->with($notification);
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);  
    }
}

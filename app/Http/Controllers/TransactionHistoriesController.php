<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransactionHistories;
use App\Product;
use App\Customer;
use App\History;
use Auth;

class TransactionHistoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->hasRole('Admin'))
        {
            $transaction_histories = TransactionHistories::get();
            $notification = array(
                'message' => 'Admin',
                'alert-type' => 'success'
            );
            return view('lich-su-giao-dich.index', compact('transaction_histories'))->with($notification);
        }
        else
        {
            $transaction_histories = TransactionHistories::where('user_id', Auth::id())->get();
            // dd($histories);
            $notification = array(
                'message' => 'nhân viên',
                'alert-type' => 'success'
            );
            return view('lich-su-giao-dich.index', compact('transaction_histories'))->with($notification);      
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = Customer::where('id', $request->customer_id)->first();
        $customer->update([
            'customer_name'     => $request->customer_name,
            'customer_address'  => $request->customer_address,
            'customer_gender'   => $request->customer_gender,
            'customer_phone'    => $request->customer_phone,
            'customer_email'    => $request->customer_email,
            // 'customer_note'     => $request->customer_note,
        ]);
        $product = Product::where('id', $request->product_id)->first();
            $product->update([
                'status' => 2,
                'note'  => 'Đã bán thành công.'
            ]);
        $history = History::where('id', $request->history_id)->first();
            $history->update([
                'consulting_status' => 1,
                'note' => 'Tư vấn thành công.'
            ]);
        $transaction_histories =  TransactionHistories::create([
            'user_id'               => \Auth::id(),
            'customer_id'           => $request->customer_id,
            'product_id'            => $request->product_id,
            'transaction_status'    => $request->transaction_status,
            'transactioned_at'      => date('Y-m-d H:i:s'),
            'deposit_amount'        => $request->deposit_amount,
            'pay_status'            => $request->pay_status,
            'paid_at'               => date('Y-m-d H:i:s'),
            'paid_by'               => $request->paid_by,
            // 'note'  => $request->note,
        ]);

            $notification_success = array(
                'message' => 'Giao dịch thành công! Vui lòng xem lại thông tin.',
                'alert-type' => 'success'
            );
        return redirect('/lich-su-giao-dich')->with($notification_success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $trans_histories = TransactionHistories::where('id', $id)->first();
        return view('lich-su-giao-dich.show', compact('trans_histories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trans = TransactionHistories::where('id',$id)->first();
            $trans->update([
                'transaction_status' => 2,
                'pay_status' => 2,
                'note' => 'Xác nhận thanh toán thành công!'
        ]);
            $notification_success = array(
                'message' => 'Xác nhận thanh toán thành công!',
                'alert-type' => 'success'
            );
        return redirect('/lich-su-giao-dich')->with($notification_success);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

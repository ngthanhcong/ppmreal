<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $a = array(1,2,3,4);
        // $b = array(1,2);
        // $arr = array();
        // // $kt = array_diff($b, $a);
        // $value = array_diff($a, $b);
        // array_push($arr, $value);
        // var_dump($arr);
        
        // for ($i = 0 ; $i <= sizeof($a) ; $i++)
        // {
            
            
        //     $key = key($value);
        //     if ($i = $key);
        //     {
        //         $arr = array_push($a[$key]);
        //     }
            
        //     print_r($arr);
        // }

        // die;
        return view('home');
    }
}

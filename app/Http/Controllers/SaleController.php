<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Customer;
use App\History;
use Carbon\Carbon;
use App\TransactionHistories;
use DB;

class SaleController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (empty($request->all()))
        {
            $products = Product::get();
            $qty_all = count($products);
            $qty = count($products);
            return view('sale.index', compact('products', 'qty_all', 'qty'));
        }
        else
        {
            $t = (\Request::has('keyword') && \Request::has('category') && \Request::has('price') && \Request::has('direction') && \Request::has('acreage'));
            $kt = ($request->keyword != null and $request->category != null and $request->price != null and $request->direction != null and $request->acreage != null);
            if ($t != true)
            {
                $products = Product::get();
                $qty_all = count($products);
                return view('sale.index', compact('products', 'qty_all'));   
            }
            else
            {
                if ($request->price == null)
                {
                    $cost_fields = [ 'min' => '1000000', 'max' => '100000000000000'];
                }
                else
                {
                    $cost_fields = __('product.cost-fields.' . 'vi')[$request['price']];
                }
                if ($request->acreage == null)
                {
                    $acreage = [ 
                        'min' => '1', 
                        'max' => '100000'
                    ];
                }
                else
                {
                    $acreage = __('product.acres.acreages')[$request['acreage']];       
                }
                
                $query = DB::table('products');
                if(!empty($request['keyword'])) $query->where('product_name', 'LIKE', '%'.$request['keyword'].'%');
                if(!empty($request['category'])) $query->where('category_id', $request['category']);
                if(!empty($request['price'])) $query->whereBetween('price', [ $cost_fields['min'], $cost_fields['max'] ] );
                if(!empty($request['acreage'])) $query->whereBetween('acreage', [ $acreage['min'], $acreage['max'] ] );
                if(!empty($request['direction'])) $query->where('direction', $request->direction );
                if(!empty($request['status'])) $query->where('status', $request->status );
                $products = $query->get();
                if ($products->count() == 0)
                {
                    $notification = array(
                        'message' => 'Không có',
                        'alert-type' => 'error'
                    );
                }
                else
                {
                    $notification = array(
                        'message' => 'Không có',
                        'alert-type' => 'error'
                    );
                }
                $qty_all = $products->count();
                $qty = $products->count();
                return view('sale.index', compact('products', 'qty_all', 'qty'))->with($notification);
            }
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // add customer name.
        $customer = Customer::create([
            'customer_name'=> $request->customer_name
        ]);
        $customer_id= Customer::where('customer_name',$request->customer_name)->value('id');
        $customer_name = $request->customer_name;
        // add history.
        $histories = History::create([
            'user_id'       => \Auth::id(),
            'customer_id'       => $customer_id,
            'product_id'        => $request->product_id,
            // 0: đang tư vấn, 1: tư vấn thành công, 2: tư vấn thất bại.
            // update sẽ thay đổi lại trạng thái.
            'consulting_status' => '0',
            'consulting_at'     => date('Y-m-d H:i:s'),
        ]);
        // change status product have id=$request->product_id.
        $product = Product::where('id',$request->product_id)->first();
            $product->update([
                // trang thái 1: đang bán, 2: đã bán, 3: đang tư vấn.
                'status' => 3,
            ]);
        $notification_success = array(
                'message' => 'Bạn có 45 phút để tư vấn cho khách hàng '. $customer_name .' ! Sau 45 không chốt giao dịch hệ thống tự động chuyển trạng thái của mảnh đất.',
                'alert-type' => 'success'
            );
        return redirect('/sale')->with($notification_success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $alias)
    {
        if ($request->user()->can('view')) {
            $product = Product::where('alias', $alias)->first();
            return view('sale.show', compact('product'));
            }
            $notification_error = array(
                'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
                'alert-type' => 'error'
            );
        return back()->with($notification_error);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

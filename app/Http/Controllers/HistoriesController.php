<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History;
use App\Customer;
use App\Product;
use App\TransactionHistories;
use Carbon\Carbon;
use Auth;

class HistoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->user()->hasRole('Admin'))
        {
            $products = Product::get();
            $qty_all = count($products);
            $qty = count($products);
            $histories = History::get();
            // check status 
            foreach ($histories as $history)
            {
                if ($history->consulting_at == 0 )
                {
                    $start  = Carbon::now();
                    $end    = Carbon::createFromFormat('Y-m-d H:i:s', $history->created_at);
                    $ktra = $start->diff($end)->format('%Y:%M:%D %H:%I:%S');
                    $kt = ($ktra <= "00:00:00 00:46:00");
                    if ( $kt == false )
                    {
                        // cập nhật trạng thái lịch sử tư vấn nếu không chốt mảnh đất.
                        $histories_update = History::where('id', $history->id)
                        ->where('consulting_status', 0)
                        ->first();
                        $histories_update->update([
                                'consulting_status' => 2,
                                'note' =>   'Thay đổi trạng thái do nhân viên không chốt giao dịch.'
                            ]);
                        // cập nhật trạng thái mảnh đất nếu không chốt mảnh đất.
                        $product_update = Product::where('id', $product_id)->first();
                        
                            $product_update->update([
                                'status' => 1,
                                'note' => 'Mảnh đất được tư vấn nhưng không chốt.'
                            ]);
                        
                        $notification_error = array(
                                'message' => 'Trạng thái mảnh đất đã thay đổi do bạn không chốt giao dịch! Vui lòng liên hệ với Admin để được hổ trợ.',
                                'alert-type' => 'info'
                            );
                        return back()->with($notification_error);
                    }
                    else
                    {
                        return view('lich-su-tu-van.index', compact('histories'));
                    }
                }   
            }
            $check_transaction_histories = TransactionHistories::where('pay_status', 1)->get();
            foreach ($check_transaction_histories as $trans)
            {
                $now = Carbon::now();
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $trans->paid_at);
                $check_date = ($now->diff($date)->format('%Y:%M:%D %H:%I:%S')) <= "00:00:01 00:01:00";
                // dd(($now->diff($date)->format('%Y:%M:%D %H:%I:%S')));
                if ($check_date == false)
                {
                    $trans_history = TransactionHistories::where('id', $trans->id)->get();
                        $trans_history->update([
                            'pay_status' => 2,
                            'note' => 'Thay đổi trạng thái do khách hàng không xác nhận thanh toán đủ số tiền.',
                        ]);
                        // cập nhật trạng thái mảnh đất nếu không chốt mảnh đất.
                        $product_update = Product::where('id', $product_id)->first();
                        $product_update->update([
                            'status' => 1,
                            'note' => 'Mảnh đất được đã chốt nhưng không xác nhận thanh toán đủ số tiền.'
                        ]);
                        // cập nhật trạng thái lịch sử tư vấn nếu không chốt mảnh đất.
                        $histories = History::where('product_id', $product_id)
                            ->where('consulting_status', 0)
                            ->whereColumn('updated_at', '=', 'created_at')
                            ->first();
                        
                        $histories->update([
                                'consulting_status' => 2,
                                'note' =>   'Thay đổi trạng thái do nhân viên không chốt giao dịch.'
                            ]);
                        $notification_error = array(
                                'message' => 'Trạng thái mảnh đất đã thay đổi do bạn không chốt giao dịch! Vui lòng liên hệ với Admin để được hổ trợ.',
                                'alert-type' => 'info'
                            );
                    return back()->with($notification_error);
                }
                else
                {
                    return view('lich-su-tu-van.index', compact('histories'));
                }
                
            }
            $notification = array(
                'message' => 'Admin',
                'alert-type' => 'success'
            );
            return view('lich-su-tu-van.index', compact('histories'))->with($notification);
        }
        else
        {
            $histories = History::where('user_id', Auth::id())->get();
            // dd($histories);
            $notification = array(
                'message' => 'nhân viên',
                'alert-type' => 'success'
            );
            return view('lich-su-tu-van.index', compact('histories'))->with($notification);      
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $history = History::where('id', $id)->first();
        return view('lich-su-tu-van.show', compact('history'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $histories = History::where('id', $id)->first();
            $histories->update([
                'consulting_status' => 2,
                'note' => "Nhân viên đã hủy bỏ.",
        ]);
        $product = Product::where('id',$histories->product_id)->first();
            $product->update([
                'status' => 1,
            ]);
        $notification = array(
            'message' => 'Đã hủy bỏ tư vấn mảnh đất ',
            'alert-type' => 'info'
        );
        return redirect('/lich-su-tu-van')->with($notification);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

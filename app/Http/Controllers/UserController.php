<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\User;
use App\UsersRole;
use App\UsersPermission;
use App\Role;
use App\Permission;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->user()->can('view-all')) {
            $users = User::get();
            $role = Role::get();
            $per = Permission::get();
            return view('user.index',array('per'=> $per,'users'=> $users,'role'=> $role));

        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    { 
        if ($request->user()->can('create')) {
            $role = Role::get();
            $per = Permission::get();
            return view('user.create', compact('role', $role),compact('per', $per));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->can('create')) {
            $error = User::where('username', strtolower($request->username))->first();
            $per_id = $request->id_per;
            if (empty($per_id))
            {
                $notification_error = array(
                    'message' => 'Vui lòng chọn một quyền trong nhóm quyền.',
                    'alert-type' => 'error'
                );
                return back()->with($notification_error);
            }
            if (isset($error))
            {
                $notification_error = array(
                    'message' => 'Tên đăng nhập đã được sử dụng! Vui lòng chọn tên khác.',
                    'alert-type' => 'error'
                );
                return back()->with($notification_error);
            }
            else
            {
                $user = User::create([
                    'username' => $request['username'],
                    'email' => $request['email'],
                    'password' => Hash::make($request['pass_first']),
                    'pass_first' => $request['pass_first'],
                    'active' =>$request['active'],
                    'fullname' => $request['fullname'],
                    'created_by' => Auth::id(),
                ]);
                $user_id= User::where('username',$request->username)->value('id');
                $user_role = UsersRole::create([
                    'user_id' => $user_id, 
                    'role_id' => $request['id_role'], 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' =>  date('Y-m-d H:i:s')
                ]);
                
                $per_id=$request->id_per;
                foreach($per_id as $key =>$val){
                $user_permission = UsersPermission::create([
                        'user_id' => $user_id, 
                        'permission_id' => $val, 
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' =>  date('Y-m-d H:i:s')
                    ]);
                }
                $notification = array(
                    'message' => 'Thêm mới thành công!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            }
            
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if ($request->user()->can('view')) {
            $user= User::where('id',$request->user)->first();
            return view('user.view', compact('user', $user));
        } 
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    public function profile(Request $request)
    {
        if ($request->user()->can('view-profile')) {
            $user= User::where('id', \Auth::id())->first();
            return view('user.profile', compact('user', $user));
        } 
        $notification_error = array(
            'message' => 'Bạn không có quyền! Vui lòng liên hệ với Admin để cập nhật thêm quyền.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    public function profileUpdate(Request $request, $id)
    {
        //  dd($request->birthday);
        if ($request->user()->can('edit-profile')) {
            // dd($request->birthday);
            // $birth = Carbon::create($request->birthday);
            $year = date('Y', strtotime($request->birthday));
            if ($year <= 1970) 
            {
                $notification = array(
                    'message' => 'Vui lòng chọn lại năm lơn hơn 1970!',
                    'alert-type' => 'info'
                );
                return redirect('/profile')->with($notification);
            }
            else
            {
                $birth = date("Y-m-d", strtotime($request->birthday));
                // dd(Carbon::now()->toDateString());
                $request->merge([
                    'birthday' => $birth,
                ]);
                $user= User::where('id',$id)->first();
                $user->update($request->all());
                $notification = array(
                    'message' => 'Cập nhật thành công!',
                    'alert-type' => 'success'
                );
                return redirect('/profile')->with($notification);
            }
        }
        else
        {
            $notification_error = array(
                'message' => 'Bạn không có quyền! Vui lòng liên hệ với Admin để cập nhật thêm quyền.',
                'alert-type' => 'error'
            );
            return back()->with($notification_error);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->user()->can('edit')) 
        {
            $user= User::where('id',$request->user)->first();
            $per = Permission::get();
            return view('user.edit',compact('user','per' ));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
       return back()->with($notification_error);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->can('edit')) {
            $birthday = date("Y-m-d", strtotime($request->birthday));
            // dd($birthday);
            $request->merge([
                'birthday' => $birthday,
            ]);
            $user= User::where('id',$id)->first();
            $user->update($request->all());
            
            $notification = array(
                'message' => 'Cập nhật thành công!',
                'alert-type' => 'success'
            );
            return redirect('/user')->with($notification);
        }
        $notification_error = array(
                'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
                'alert-type' => 'error'
            );    
       return back()->with($notification_error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->user()->can('delete')) {
            $user = User::findOrFail($request->id);
            if ($user->id == Auth::id())
            {
                $notification_error = array(
                    'message' => 'Bạn không có quyền xóa tài khoản của chính mình.',
                    'alert-type' => 'error'
                );
                return back()->with($notification_error);
            }
            else
            {
                $user->delete();
                $notification = array(
                    'message' => 'Xóa bỏ thành công!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            }
            
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_error);
    }

    public function activeStatus(Request $request, $id)
    {
        if ($request->user()->can('edit'))
        {
            $user= User::where('id',$id)->first();
                $user->update([
                    'active' => 1,
                ]);
                $notification = array(
                    'message' => 'Kích hoạt tài khoản thành công!',
                    'alert-type' => 'success'
                );
            return redirect('/user')->with($notification);
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_error);
    }
}

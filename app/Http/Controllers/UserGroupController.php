<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserGroup;

class UserGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usergroups = UserGroup::get();
        return view('usergroup.index', compact('usergroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usergroup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = UserGroup::where('group_name', $request->group_name)->first();
        if (isset($error))
        {
            $notification = array(
                'message' => 'Tên nhóm đã tồn tại!',
                'alert-type' => 'error'
            );
        }
        else
        {
            $user_group = UserGroup::create($request->all());
            $notification = array(
                'message' => 'Thêm mới thành công!',
                'alert-type' => 'success'
            );
           
        }
        return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('usergroup.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_group = UserGroup::where('id', $id)->first();
        $user_group->update($request->all());
        $notification = array(
            'message' => 'Cập nhật thành công!',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        return view('usergroup.delete');
    }

    public function destroy($id)
    {
        $user_group = UserGroup::where('id', $id)->first();
        $user_group->delete();
        $notification = array(
            'message' => 'Xóa bỏ thành công!',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }
}

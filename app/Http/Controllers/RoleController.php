<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\UsersRole;
use Carbon\Carbon;

class RoleController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->user()->can('view-all')) 
        {
            $role = Role::get();
            return view('role.index', compact('role', $role));
        } 
         $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_error);  
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->user()->can('create')) {
            return view('role.create');
            }
            $notification_error = array(
                'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
                'alert-type' => 'error'
            );
            return back()->with($notification_error);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->can('create')) {
            $error = Role::where('slug', $request->slug)->first();
            if (isset($error))
            {
                $notification = array(
                    'message' => 'Tên vừa nhập đã tồn tại!',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }
            else
            {
                $role = Role::create([
                    'slug' => $request['slug'], 
                    'name' => $request['name'], 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' =>  date('Y-m-d H:i:s')
                ]);
                $notification_success = array(
                    'message' => 'Thêm mới thành công!',
                    'alert-type' => 'success'
                );
                return redirect('/role')->with($notification_success);
            }
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_error);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->user()->can('view')) {
            $role=  Role::where('id',$request->role)->first();
           return view('role.view', compact('role', $role));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->user()->can('edit')) {
            $role= Role::where('id',$request->role)->first();
        return view('role.edit',compact('role', $role));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_error);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->can('edit')) 
        {
            $role= Role::where('id',$id)->first();
            $role->update($request->all());
            $notification_success = array(
                'message' => 'Lưu lại thành công!',
                'alert-type' => 'success'
            );
            return redirect('/role')->with($notification_success);
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->user()->can('delete')) {
            $role= Role::findOrFail($request->id);
            $error_role = UsersRole::where('role_id', $role->id)->get();
            if (isset($error_role))
            {
                $notification_error = array(
                    'message' => 'Vui lòng xóa tài khoản này của người dùng trước khi xóa nhóm người dùng!',
                    'alert-type' => 'error'
                );
                return back()->with($notification_error);  
            }
            else
            {
                $role->delete();
                $notification_success = array(
                    'message' => 'Xóa bỏ thành công!',
                    'alert-type' => 'success'
                );
                return back()->with($notification_success);
            }
            
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);  
    }
}

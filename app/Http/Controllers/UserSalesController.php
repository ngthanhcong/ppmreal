<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\User;
use App\UsersRole;
use App\UsersPermission;
use App\Role;
use App\Permission;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Validator;

class UserSalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('created_by', Auth::id())->get();

        $role = Role::get();
        $per = Permission::get();
        return view('user-sale.index',array('per'=> $per,'users'=> $users,'role'=> $role));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::get();
        $per = Permission::get();
        return view('user-sale.create', compact('role', $role),compact('per', $per));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = User::where('username', strtolower($request->username))
        ->orwhere('email', $request->email)
        ->first();
        if (isset($error))
        {
            $notification_error = array(
                'message' => 'Tên đăng nhập hoặc Email đã được sử dụng!',
                'alert-type' => 'error'
            );
            return back()->with($notification_error);
        }
        else
        {
            $messages = [
            'fullname.required'     => __('user-sale.fullname') .' là bắt buộc',
            'username.required'     => __('user-sale.username') . ' là bắt buộc',
            'password.required'     => __('user-sale.password') . ' là bắt buộc',
            'email.required'        => __('user-sale.email') . ' là bắt buộc',
            ];
            $validator = Validator::make($request->all(), [
                'fullname'      => 'required',
                'username'      => 'required',
                'password'      => 'required',
                'email'         => 'required',
            ], $messages );
            $notification = array(
                'message' => 'Vui lòng không bỏ trống các trường có đâu (*)',
                'alert-type' => 'info'
            );
            if ($validator->fails()) {
            return redirect('/user-sale')->with($notification)
                        ->withErrors($validator)
                        ->withInput();
            }
            $user = User::create([
                'username' => $request['username'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'pass_first' => $request->password,
                'active' => 0,
                'fullname' => $request['fullname'],
                'created_by' => Auth::id(),
                'note' =>'Tài khoản được tạo bởi nhân viên '. Auth::user()->fullname . '.' ,
            ]);
            $user_id= User::where('username',$request->username)->value('id');
            $user_role = UsersRole::create([
                'user_id' => $user_id, 
                'role_id' => 1, 
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' =>  date('Y-m-d H:i:s')
            ]);
                        
            $per_id= [3,5,6,7];
            foreach($per_id as $key =>$val){
                $user_permission = UsersPermission::create([
                    'user_id' => $user_id, 
                    'permission_id' => $val, 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' =>  date('Y-m-d H:i:s')
                ]);
            }
            $notification = array(
                'message' => 'Thêm nhân viên cấp dưới thành công! Vui lòng chờ để Admin xét duyệt tài khoản.',
                'alert-type' => 'success'
                );
            return back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user= User::where('id',$id)->first();
        return view('user-sale.view', compact('user', $user));      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user= User::where('id',$id)->first();
        $per = Permission::get();
        return view('user-sale.edit',compact('user','per' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user= User::where('id',$id)->first();
            $birthday = date("Y-m-d", strtotime($request->birthday));
            if ($request->active == null )
            {
                $active = $user->active;
            }
            else
            {
                $active = $user->active;
            }
            $request->merge([
                'birthday' => $birthday,
                'active'   => $active
            ]);
            // $user= User::where('id',$id)->first();
            $user->update($request->all());

            $notification = array(
                'message' => 'Cập nhật thành công!',
                'alert-type' => 'success'
            );
            return redirect('/user-sale')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
            $notification = array(
                'message' => 'Xóa bỏ thành công!',
                'alert-type' => 'success'
        );
        return back()->with($notification);
    }
}

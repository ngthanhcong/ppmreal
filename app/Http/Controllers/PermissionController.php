<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\UsersPermission;

class PermissionController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->user()->can('view-all')) 
        {
            $per = Permission::get();
            return view('permission.index', compact('per', $per));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($request->user()->can('create')) {
            return view('user.create');
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->can('create')) 
        {
            $error = Permission::where('slug', $request->slug)->first();
            if (isset($error))
            {
                $notification = array(
                    'message' => 'Tên vừa nhập đã tồn tại!',
                    'alert-type' => 'error'
                );
                return  back()->with($notification);
            }
            else
            {
                $per = Permission::create([
                    'slug' => $request['slug'], 
                    'name' => $request['name'], 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' =>  date('Y-m-d H:i:s')
                ]);
                $notification_success = array(
                    'message' => 'Thêm thành công!',
                    'alert-type' => 'success'
                );
                return redirect('/permission')->with($notification_success);
            }
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_error);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->user()->can('view')) {
            $per= Permission::where('id',$request->permission)->first();
           return view('permission.view', compact('per', $per));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return  back()->with($notification_success);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->user()->can('edit')) {
            $per= Permission::where('id',$request->permission)->first();
        return view('permission.edit',compact('per', $per));
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
       return  back()->with($notification_error);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->can('edit'))
        {
            $per= Permission::where('id',$id)->first();
            $per->update($request->all());
            $notification = array(
                'message' => 'Cập nhật thành công!',
                'alert-type' => 'success'
            );
        return redirect('/permission')->with($notification);
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
       return back()->with($notification_error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->user()->can('delete')) {
            $per= Permission::findOrFail($request->id);
            $error_per = UsersPermission::where('permission_id', $per->id)->first();
            if (isset($error_per))
            {
                $notification_error = array(
                    'message' => 'Vui lòng xóa quyền này của người dùng trước khi xóa nhóm quyền!',
                    'alert-type' => 'error'
                );
                return back()->with($notification_error);
            }
            else
            {
                $per->delete();
                $notification = array(
                    'message' => 'Xóa bỏ thành công!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            }
        }
        $notification_error = array(
            'message' => 'Bạn không có quyền! Thao tác này chỉ Admin mới được dùng.',
            'alert-type' => 'error'
        );
        return back()->with($notification_error);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class SaleEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->hasRole('Employee')) {
            return redirect('sale');
        }
    
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permission = null)
    {
        if(!$request->user()->hasRole($role) == 'Employees') 
        {
            if (Auth::check() && Auth::user()->active == 1)
            // redirect to page sale
            return redirect('/sale');
            else
            {
                // redirect to sale when Employee login
                Auth::logout();
                return redirect('/')->with('flash-message', 'Tài khoản chưa được kích hoạt vui lòng liên hệ Admin!');
            }
            
            
           
        }
        else
        {
            if(!$request->user()->hasRole($role) == 'Employees' and !$request->user()->hasRole($role) == 'Admin') 
            {
                // redirect to 404 
                abort(404);
            }
        }
    
        if($permission !== null && !$request->user()->can($permission)) 
        {
            abort(404);
        }
        return $next($request);
    }
}

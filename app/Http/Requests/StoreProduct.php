<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'       => 'required',
            'ward_id'           => 'required',
            'user_id'           => 'required',
            'product_name'      => 'required|max:191',
            'alias'             => 'required',
            'avatar'            => '',
            'alt'               => '',
            'images'            => '',
            'summary'           => '',
            'content'           => '',
            'price'             => '',
            'acreage'           => 'required',
            'area'              => '',
            'length'            => '',
            'width'             => '',
            'direction'         => 'required',
            'status'            => 'required',
            'lat'               => '',
            'lng'               => '',
            'note'              => '',
        ];
    }

    // attributes
    public function attributes()
    {
        return __('product');
    }
}
